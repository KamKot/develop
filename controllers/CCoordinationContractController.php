<?php
namespace controllers;
use models\CCoordinationContractModel;
use system\AController;
use system\CVarDump;
use system\MonCms;

/**
 * Created by PhpStorm.
 * User: malyshevis
 * Date: 07.02.17
 * Time: 14:02
 */

class CCoordinationContractController extends AController
{
    public $model = null;

    public function loadModel()
    {
        if ($this->model === null)
        {
            $this->model = new CCoordinationContractModel(MonCms::$db, MonCms::$config);
        }

        return $this->model;
    }

    public function checkRights()
    {
        $rights = [];

        if (check_group('cp_coordination_view'))
            $rights['coordinationView'] = true;

        return $rights;
    }

    public function actionIndex()
    {
        $model = $this->loadModel();

        if (isset($_GET['coordinationId']))
        {
            $model->setAttribute('coordinationId', $_GET['coordinationId']);
        }

        if ($model->validate('coordinationId'))
        {

            $rights = $this->checkRights();
            $coordination = $model->viewCoordination();
            $documentType = $model->getDocumentType();
            $coordinationFollower = $model->getFollower();
            $countFollower = $model->countFollower();
            $coordinationStatus = $model->getStatus();
            $coordinationComment = $model->getComment();

            return $this->render('coordinationContract/coordinationView.tpl', ['coordination'           => $coordination['clientCoordination'],
                                                                               'coordinationComment'    => $coordinationComment,
                                                                               'coordinationFile'       => $coordination['clientCoordinationFile'],
                                                                               'coordinationStatus'     => $coordinationStatus,
                                                                               'documentType'           => $documentType,
                                                                               'countFollower'          => $countFollower,
                                                                               'coordinationFollower'   => $coordinationFollower,
                                                                               'currentUser'            => MonCms::$user->userId,
                                                                               'brandId'                => $coordination['clientCoordination']['brandId'],
                                                                               'clientId'               => $coordination['clientCoordination']['clientId'],
                                                                               'rights'                 => $rights
                                                                                ]);
        }
    }


    /**
     * ��������� ���� �� ������ ����� ����������� �����. ���� ����, �� ������� �� ����������, � ��������� � ������������
     */
    public function actionGetClientInfo()
    {
        $model = $this->loadModel();
        if (isset($_POST['clientId']) and isset($_POST['brandId']))
        {
            $model->setAttribute('clientId', $_POST['clientId']);
            $model->setAttribute('brandId', $_POST['brandId']);
            if ($model->validate('clientId') and $model->validate('brandId'))
            {
                $json['documentType'] = $model->getDocumentType();
                $json['order'] = $model->getOrderInfo();
                echo json_encode(iconv_deep('windows-1251', 'utf-8', $json));
                exit;
            }
        }
    }

    /**
     * ������� ����� ������������
     */
    public function actionCreateNewCoordination()
    {
        $model = $this->loadModel();
        if (isset($_POST['createCoordination']))
        {
            $model->setAttributes($_POST['createCoordination']);
            if ($model->validate('createCoordination'))
            {
                $coordinationId = $model->createNewCoordination();
                header("Location: ./index.php?mod=coordinationContract&coordinationId=".$coordinationId);
            }
        }
    }

    public function actionSaveCoordination()
    {
        $model = $this->loadModel();
        if (isset($_POST['saveCoordination']))
        {
            $model->setAttributes($_POST['saveCoordination']);
            $model->setAttribute('lastTypeDocumentDescription',$_POST['lastTypeDocumentDescription']);
            $model->setAttribute('lastStatusDescription',$_POST['lastStatusDescription']);
            $model->setAttribute('contractNo',$_POST['contractNo']);
            if ($model->validate('saveCoordination'))
            {
                $model->saveCoordination();
                header("Location: ./index.php?mod=coordinationContract&coordinationId=".$model->coordinationId);
            }
        }
    }

    /**
     * ��������� ���������
     */
    public function actionAddFollower()
    {
        $model = $this->loadModel();
        if (isset($_POST['coordinationId']) and isset($_POST['followerUserId']) and isset($_POST['contractNo']))
        {
            $model->setAttribute('followerUserId', $_POST['followerUserId']);
            $model->setAttribute('coordinationId', $_POST['coordinationId']);
            $model->setAttribute('contractNo', iconv_deep($_POST['contractNo'],'utf-8','windows-1251'));
            if ($model->validate('followerUserId') and $model->validate('coordinationId'))
            {
                $model->addFollower();
                echo json_encode(1);
                exit();
            }
        }
    }


    /**
     * ������� ���������
     */
    public function actionDeleteFollower ()
    {
        $model = $this->loadModel();
        if (isset($_GET['coordinationId']) and isset($_GET['followerUserId']) and isset($_GET['contractNo']) )
        {
            $model->setAttribute('followerUserId', $_GET['followerUserId']);
            $model->setAttribute('coordinationId', $_GET['coordinationId']);
            $model->setAttribute('contractNo', $_GET['contractNo']);
            if ($model->validate('followerUserId') and $model->validate('coordinationId'))
            {
                $model->deleteFollower();
                header("Location: ./index.php?mod=coordinationContract&coordinationId=".$_GET['coordinationId']);
            }
        }
    }

    /**
     * �������� �����
     */
    public function actionFile()
    {
        global $err_msg;
        $model = $this->loadModel();
        if (isset($_POST['coordinationFile']))
        {
            $model->setAttributes($_POST['coordinationFile']);
            $model->setAttribute('uploadFileCoordinationDescription',iconv('utf-8','windows-1251',$_POST['uploadFileCoordinationDescription']));
            $model->setAttribute('contractNo',$_POST['contractNo']);
            if ($model->validate('coordinationFile'))
            {
                $uploadFile = $model->uploadFile();
                if($uploadFile)
                {
                    echo json_encode(intval($model->coordinationId));
                }
                else
                    echo json_encode(0);
                exit();
            }
            else
            {
                $err_msg = $model->getErrors()->getErrorListString();
            }
        }
    }


    /**
     * ������������� ����
     */
    public function actionCoordinateFile()
    {
        global $err_msg;
        $model = $this->loadModel();
        if (isset($_POST['coordinationId']) and isset($_POST['fileId']))
        {
            $model->setAttribute('coordinationId',$_POST['coordinationId']);
            $model->setAttribute('fileId',$_POST['fileId']);
            if ($model->validate('coordinationId') and $model->validate('fileId'))
            {
                $model->coordinateFile();
                echo json_encode(1);
                exit();
            }
            else
            {
                $err_msg = $model->getErrors()->getErrorListString();
            }
        }
    }

    public function actionAddComment()
    {
        global $err_msg;
        $model = $this->loadModel();
        if (isset($_POST['comment']) and isset($_POST['commentText']) and isset($_POST['answer']))
        {
            $model->setAttribute('commentText',$_POST['commentText']);
            $model->setAttributes($_POST['answer']);
            $model->setAttributes($_POST['comment']);
            if ($model->validate('comment'))
            {
                $comment = $model->addComment();
                if(!$comment)
                    $err_msg = '����������� �� ����� ���� ������';
                else
                    header("Location: ./index.php?mod=coordinationContract&coordinationId=".$model->coordinationId);
            }
            else
            {
                $err_msg = $model->getErrors()->getErrorListString();
            }
        }
    }
}