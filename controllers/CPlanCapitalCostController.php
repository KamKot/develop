<?php
namespace controllers;

use models\CPlanCapitalCostModel;
use system\AController;
use system\CPagination;
use system\CVarDump;
use system\MonCms;

/**
 * Created by PhpStorm.
 * User: malyshevis
 * Date: 17.01.17
 * Time: 16:22
 */
class CPlanCapitalCostController extends AController
{
    public $planCapitalCostModel = null;

    /**
     * ������������� ������
     * @return CPlanCapitalCostModel
     */
    public function loadModel()
    {
        if ($this->planCapitalCostModel === null)
            $this->planCapitalCostModel = new CPlanCapitalCostModel(MonCms::$db, MonCms::$config);

        return $this->planCapitalCostModel;
    }

    public function checkAccess()
    {
        return check_group('planCapitalCost') ? true : false;
    }

    public function error($msg)
    {
        return $this->render('pok_analyze/error.tpl', ['errorString' => $msg]);
    }

    public function actionIndex()
    {
        if (!$this->checkAccess())
            return $this->error('����������� ����� ������ � ������');

        global $err_msg;
        $model = $this->loadModel();
        $csv = './index.php?mod=planCapitalCost&act=csv';
        if (isset($_GET['page']))
            $model->setAttribute('page', $_GET['page']);

        if (isset($_GET['filter']))
        {
            // ���� ��������� ���� ���� �� ������� ��� ����������� ���� ���
            if ($_GET['filter']['isFinishDate'])
            {
                $_GET['filter']['isFinishDate'] = '1';
                $_GET['filter']['planDateFinishBuild'] = null;
            }
            else
                $_GET['filter']['isFinishDate'] = '0';

            // ���� ��������� ���� ���� �� ������� ��� ������� ���������
            if ($_GET['filter']['isDateLineDepartment'])
            {
                $_GET['filter']['isDateLineDepartment'] = '1';
                $_GET['filter']['planDateLineDepartment'] = null;
            }
            else
                $_GET['filter']['isDateLineDepartment'] = '0';

            if ($_GET['filter']['isPlanDateInstallService'])
            {
                $_GET['filter']['isPlanDateInstallService'] = '1';
                $_GET['filter']['planDateInstallService'] = null;
            }
            else
                $_GET['filter']['isPlanDateInstallService'] = '0';

            //���� ��������� ���� ��� ���
            if ($_GET['filter']['isTeoId'])
            {
                $_GET['filter']['isTeoId'] = '1';
            }
            else
                $_GET['filter']['isTeoId'] = '0';

            // ���� ��������� ���� �������� �� ������
            if ($_GET['filter']['isManager'])
            {
                $_GET['filter']['isManager'] = '1';
                $_GET['filter']['manager'] = null;
            }
            else
                $_GET['filter']['isManager'] = '0';

            // ���� ��������� ���� �������� �� ������
            if ($_GET['filter']['isProjectManager'])
            {
                $_GET['filter']['isProjectManager'] = '1';
                $_GET['filter']['projectManager'] = null;
            }
            else
                $_GET['filter']['isProjectManager'] = '0';


            if (isset($_GET['filter']['radio']))
            {
                switch ($_GET['filter']['radio'])
                {
                    //����� ����������
                    case 'withOutFilter':
                        $_GET['filter']['withOutFilter'] = '1';
                        break;

                    // ���� ��� ������� ������ �����, � ������� ��� �������� � ���
                    case 'urWithoutSmr':
                        $_GET['filter']['urWithoutSmr'] = '1';
                        break;

                    // ���� ������ ��� �� ���
                    case 'bcRtk':
                        $_GET['filter']['bcRtk'] = '1';
                        break;

                    // ���� ������������ ����
                    case 'networkModernization':
                        $_GET['filter']['networkModernization'] = '1';
                        break;

                    default:
                        break;

                }
            }


            $model->setAttributes($_GET['filter']);
            $model->setAttribute('seller', $_GET['seller']);
            if ($model->validate('filter'))
            {
                foreach ($_GET['filter'] as $key => $val)
                    $csv .= '&filter['.$key.']='.$val;

                $model->isFilter = 1;
                $filter = $_GET['filter'];
                //$count = $model->countPlan();
                $model->pageLen = MonCms::$config['page_count'];
                $model->start = ($model->page - 1) * $model->pageLen;
                //$pages = new CPagination(ceil($count / $model->pageLen));
                $planCapitalCost = $model->getAllPlan()['planCapital'];
                $totalCost = $model->getAllPlan()['totalCost'];
                #CVarDump::dump($planCapitalCost);
                #exit;
            }
            else
                $err_msg = $model->gerErrorsListString();
        }

        return $this->render('planCapitalCost/planCapitalCostView.tpl', ['planCapitalCost'          => $planCapitalCost,
                                                                         'csv'                      => $csv,
                                                                         'filter'                   => $filter,
                                                                         'totalCost'                => $totalCost,
                                                                         //'pages'                    => $pages,
                                                                         'filterAddress'            => $_GET['filterAddress'],
                                                                         'isFinishDate'             => $model->isFinishDate,
                                                                         'isDateLineDepartment'     => $model->isDateLineDepartment,
                                                                         'isManager'                => $model->isManager,
                                                                         'isTeoId'                  => $model->isTeoId,
                                                                         'isPlanDateInstallService' => $model->isPlanDateInstallService,
                                                                         'isProjectManager'         => $model->isProjectManager,
                                                                         'seller'                   => $model->seller]);
    }

    public function actionCsv()
    {
        if (!$this->checkAccess())
            return $this->error('����������� ����� ������ � ������');

        $model = $this->loadModel();
        global $err_msg;

        $model->isCsv = 1;
        if (isset($_GET['filter']))
        {
            $model->isFilter = 1;
            $model->setAttributes($_GET['filter']);
            if ($model->validate('filter'))
            {
                $planCapital = $model->getAllPlan()['planCapital'];
                header('Content-type: text/csv');
                header('Expires: '.gmdate('D, d M Y H:i:s').' GMT');
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Pragma: public');
                header('Content-Disposition: attachment; filename="planCapitalCost_'.date('Y.m.d').'.csv"');

                echo '#;';
                echo '���;';
                echo '�������� ���;';
                echo '�����;';
                echo '�������;';
                echo '�����;';
                echo '��������;';
                echo '��� ������;';
                echo '�������;';
                echo '��.���� ��������� �������;';
                echo '����.���� ��������� �������;';
                echo '���� ����-�������;';
                echo '���� ������ ������;';
                echo '����.�����;';
                echo '����.����.������;';
                echo '��.����� ������������;';
                echo '����.����� ������������;';
                echo '��.����� ���;';
                echo '����.����� ���;';
                echo 'Capex(���);';
                echo 'Opex(���);';
                echo '������ ���;';
                echo '����. EBITDA;';
                echo '����. ���� �����������;';
                echo '����. EBITDA;';
                echo '����. ���� �����������;';
                echo '����� ������;';
                echo '���������;';
                echo "\n";

                $i = 1;
                $planDeviceCost = 0;
                $factDeviceCost = 0;
                $planSmrCost = 0;
                $factSmrCost = 0;
                $planSumIncome = 0;
                $averageSum = 0;
                $capex = 0;
                $opex = 0;
                $deltaSmrCost = 0;
                foreach ($planCapital as $data)
                {
                    $planDeviceCost += $data['planDeviceCost'];
                    $planSmrCost += $data['planSmrCost'];
                    $factSmrCost += $data['factSmrCost'];
                    $capex += $data['capex'];
                    $opex += $data['opex'];
                    $deltaSmrCost += ($data['planSmrCost'] - $data['factSmrCost']);
                    $factDeviceCost += $data['factDeviceCost'];
                    $planSumIncome += $data['planSumIncome'];
                    $averageSum += $data['averageSum'];
                }

                echo "\n";
                echo "�����;";
                echo ";";
                echo ";";
                echo ";";
                echo ";";
                echo ";";
                echo ";";
                echo ";";
                echo ";";
                echo ";";
                echo ";";
                echo ";";
                echo ";";
                echo number_format($planSumIncome, 2, ',', ' ').";";
                echo number_format($averageSum, 2, ',', ' ').";";
                echo number_format($planDeviceCost, 2, ',', ' ').";";
                echo number_format($factDeviceCost, 2, ',', ' ').";";
                echo number_format($planSmrCost, 2, ',', ' ').";";
                echo number_format($factSmrCost, 2, ',', ' ').";";
                echo number_format($capex, 2, ',', ' ').";";
                echo number_format($opex, 2, ',', ' ').";";
                echo number_format($deltaSmrCost, 2, ',', ' ').";";
                echo ";";
                echo ";";
                echo ";";
                echo ";";
                echo ";";
                echo ";";
                echo ";";
                echo "\n";
                echo "\n";

                foreach ($planCapital as $data)
                {
                    echo $i.';';
                    echo $data['teoId'].';';
                    echo str_replace(';', '', str_replace('quot;', '"', str_replace('&amp;', '', str_replace('&quot;', '"', $data['teoProjectName'])))).';';
                    echo str_replace('&quot;', '"', $data['address']).';';
                    echo $data['contract'].';';
                    echo str_replace(';', '', str_replace('quot;', '"', str_replace('&amp;', '', str_replace('&quot;', '"', $data['brand'])))).';';
                    echo $data['manager'].';';
                    if($data['al'] == '1')
                        echo "����������� ����� ";

                    if($data['e1'] == '1')
                        echo "����� �1 ";

                    if($data['int'] == '1')
                        echo "�������� ";

                    if($data['kpd'] == '1')
                        echo "��� ";
                    echo ";";
                    echo $data['sphere'].';';
                    echo $data['planSmrDate'].';';
                    echo $data['factSmrDate'].';';
                    echo $data['dateInvoice'].';';

                    if($data['startPayDate'] != null)
                        echo $data['startPayDate'].';';
                    else
                        echo ";";

                    echo number_format($data['planSumIncome'], 2, ',', ' ').';';
                    echo number_format($data['averageSum'], 2, ',', ' ').';';
                    echo number_format($data['planDeviceCost'], 2, ',', ' ').';';

                    if ($data['factDeviceCost'] != null or $data['factDeviceCost'] != 0)
                    {
                        echo number_format($data['factDeviceCost'], 2, ',', ' ').";";
                    }
                    else
                    {
                        echo '0,0;';
                    }

                    echo number_format($data['planSmrCost'], 2, ',', ' ').';';
                    echo number_format($data['factSmrCost'], 2, ',', ' ').';';
                    echo number_format($data['capex'], 2, ',', ' ').';';
                    echo number_format($data['opex'], 2, ',', ' ').';';
                    echo number_format(($data['planSmrCost'] - $data['factSmrCost']), 2, ',', ' ').';';

                    if(is_numeric($data['planEbitdaRent']))
                        echo number_format($data['planEbitdaRent'], 2, ',', ' ').'%;';
                    else
                        echo'������������� EBITDA;';

                    if(is_numeric($data['planPayback']))
                        echo number_format($data['planPayback'], 2, ',', ' ').';';
                    else
                        echo $data['planPayback'].';';

                    if(is_numeric($data['factEbitdaRent']))
                        echo number_format($data['factEbitdaRent'], 2, ',', ' ').'%;';
                    else
                        echo'������������� EBITDA;';

                    if(is_numeric($data['factPayback']))
                        echo number_format($data['factPayback'], 2, ',', ' ').';';
                    else
                        echo $data['factPayback'].';';

                    if($data['quantity'] != null)
                        echo $data['quantity'].';';
                    else
                        echo '0.00;';

                    echo $data['seller'].';';
                    echo "\n";
                    $i++;
                }
                exit;
            }
            else
            {
                $err_msg = $model->getErrors()->getErrorListString();
            }
        }
    }
}