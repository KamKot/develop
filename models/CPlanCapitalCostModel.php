<?php
namespace models;

use system\AModel;
use system\CVarDump;
use system\MonCms;

ini_set('memory_limit', '1024M');

/**
 * Created by PhpStorm.
 * User: malyshevis
 * Date: 17.01.17
 * Time: 16:22
 */
class CPlanCapitalCostModel extends AModel
{

    public $dateFrom;
    public $dateTo;
    public $houseId;
    public $planDateInstallService;
    public $isPlanDateInstallService;
    public $planDateFinishBuild;
    public $planDateLineDepartment;
    public $isFilter;
    public $teoId;
    public $sellerId;
    public $seller;
    public $isCsv;
    public $page = 1;
    public $pageLen;
    public $start;
    public $isFinishDate;
    public $isDateLineDepartment;
    public $isManager;
    public $isProjectManager;
    public $isTeoId;
    public $teoProjectName;
    public $manager;
    public $projectManager;
    public $factSmrSort;
    public $planSmrSort;
    public $planDeviceSort;
    public $factDeviceSort;
    public $deltaSmrSort;
    public $capexSort;
    public $opexSort;
    public $ebitdaRentSort;
    public $factEbitdaRentSort;
    public $paybackSort;
    public $factPaybackSort;
    public $urWithoutSmr;
    public $bcRtk;
    public $networkModernization;
    public $withOutFilter;
    public $radio;


    public function rules()
    {
        return [
            'filter' => [['subject'    => $this->houseId,
                          'validator'  => 'regexp',
                          'pattern'    => '/[0-9]{5,6}/',
                          'allowEmpty' => true,
                          'error'      => '�������� �������� ������',
                          'errorId'    => 'houseId'],

                         ['subject'    => $this->sellerId,
                          'validator'  => 'id',
                          'allowEmpty' => true,
                          'error'      => '�������� �������� id ����������',
                          'errorId'    => 'sellerId'],

                         ['subject'    => $this->dateFrom,
                          'validator'  => 'datetimeformat',
                          'format'     => 'Y-m-d',
                          //'allowEmpty' => true,
                          'error'      => '�������� �������� ����',
                          'errorId'    => 'dateFrom'],

                         ['subject'    => $this->dateTo,
                          'validator'  => 'datetimeformat',
                          'format'     => 'Y-m-d',
                          //'allowEmpty' => true,
                          'error'      => '�������� �������� ����',
                          'errorId'    => 'dateTo'],

                         ['subject'    => $this->planDateInstallService,
                          'validator'  => 'datetimeformat',
                          'format'     => 'Y-m-d',
                          'allowEmpty' => true,
                          'error'      => '�������� �������� �������',
                          'errorId'    => 'planDateInstallService '],

                         ['subject'   => $this->isPlanDateInstallService,
                          'validator' => 'range',
                          'range'     => ['0', '1'],
                          'error'     => '�������� �������� �������',
                          'errorId'   => 'isPlanDateInstallService '],

                         ['subject'    => $this->planDateFinishBuild,
                          'validator'  => 'datetimeformat',
                          'format'     => 'Y-m-d',
                          'allowEmpty' => true,
                          'error'      => '�������� �������� �������',
                          'errorId'    => 'planDateFinishBuild'],

                         ['subject'    => $this->planDateLineDepartment,
                          'validator'  => 'datetimeformat',
                          'format'     => 'Y-m-d',
                          'allowEmpty' => true,
                          'error'      => '�������� �������� �������',
                          'errorId'    => 'planDateLineDepartment'],

                         ['subject'    => $this->teoId,
                          'validator'  => 'regexp',
                          'pattern'    => '/[[:digit:]]*$/',
                          'allowEmpty' => true,
                          'error'      => '�������� �������� id ���',
                          'errorId'    => 'teoId'],

                         ['subject'   => $this->isFinishDate,
                          'validator' => 'range',
                          'range'     => ['0', '1'],
                          'error'     => '�������� �������� id �������',
                          'errorId'   => 'isFinishDate'],

                         ['subject'    => $this->factSmrSort,
                          'validator'  => 'range',
                          'range'      => ['asc', 'desc'],
                          'allowEmpty' => true,
                          'error'      => '�������� �������� ���������� ����.����� ���',
                          'errorId'    => 'factSmrCost'],

                         ['subject'    => $this->planSmrSort,
                          'validator'  => 'range',
                          'range'      => ['asc', 'desc'],
                          'allowEmpty' => true,
                          'error'      => '�������� �������� ���������� ����.����� ���',
                          'errorId'    => 'planSmrCost'],

                         ['subject'    => $this->planDeviceSort,
                          'validator'  => 'range',
                          'range'      => ['asc', 'desc'],
                          'allowEmpty' => true,
                          'error'      => '�������� �������� ���������� ����.����� ������������',
                          'errorId'    => 'planDeviceSort'],

                         ['subject'    => $this->factDeviceSort,
                          'validator'  => 'range',
                          'range'      => ['asc', 'desc'],
                          'allowEmpty' => true,
                          'error'      => '�������� �������� ���������� ����.����� ������������',
                          'errorId'    => 'factDeviceSort'],

                         ['subject'    => $this->deltaSmrSort,
                          'validator'  => 'range',
                          'range'      => ['asc', 'desc'],
                          'allowEmpty' => true,
                          'error'      => '�������� �������� ���������� ������ ���',
                          'errorId'    => 'deltaSmrSort'],

                         ['subject'    => $this->capexSort,
                          'validator'  => 'range',
                          'range'      => ['asc', 'desc'],
                          'allowEmpty' => true,
                          'error'      => '�������� �������� ���������� capex',
                          'errorId'    => 'capex'],

                         ['subject'    => $this->opexSort,
                          'validator'  => 'range',
                          'range'      => ['asc', 'desc'],
                          'allowEmpty' => true,
                          'error'      => '�������� �������� ���������� opex',
                          'errorId'    => 'opexSort'],

                         ['subject'    => $this->ebitdaRentSort,
                          'validator'  => 'range',
                          'range'      => ['asc', 'desc'],
                          'allowEmpty' => true,
                          'error'      => '�������� �������� ���������� ebitda',
                          'errorId'    => 'ebitdaRentSort'],

                         ['subject'    => $this->paybackSort,
                          'validator'  => 'range',
                          'range'      => ['asc', 'desc'],
                          'allowEmpty' => true,
                          'error'      => '�������� �������� ���������� ����� �����������',
                          'errorId'    => 'paybackSort'],

                         ['subject'    => $this->factEbitdaRentSort,
                          'validator'  => 'range',
                          'range'      => ['asc', 'desc'],
                          'allowEmpty' => true,
                          'error'      => '�������� �������� ���������� ebitda',
                          'errorId'    => 'factEbitdaRentSort'],

                         ['subject'    => $this->factPaybackSort,
                          'validator'  => 'range',
                          'range'      => ['asc', 'desc'],
                          'allowEmpty' => true,
                          'error'      => '�������� �������� ���������� ����� �����������',
                          'errorId'    => 'factPaybackSort'],

                         ['subject'    => $this->teoProjectName,
                          'validator'  => 'regexp',
                          'pattern'    => '/(^[�-��-ߨ�]{1,100}$)|(^[�-��-ߨ�]+[0-9]{1,100}$)|(^[0-9]+[�-��-ߨ�]{1,100}$)|([0-9]{1,100})$/',
                          'allowEmpty' => true,
                          'error'      => '��������� ����� ������',
                          'errorId'    => 'teoProjectName'],

                         ['subject'    => $this->manager,
                          'validator'  => 'regexp',
                          'pattern'    => '/^[a-zA-Z�-���-ߨ\s\-]+$/u',
                          'allowEmpty' => true,
                          'error'      => '������������ ������ ���',
                          'errorId'    => 'manager'],

                         ['subject'    => $this->projectManager,
                          'validator'  => 'regexp',
                          'pattern'    => '/^[a-zA-Z�-���-ߨ\s\-]+$/u',
                          'allowEmpty' => true,
                          'error'      => '������������ ������ ���',
                          'errorId'    => 'projectManager'],

                         ['subject'    => $this->radio,
                          'validator'  => 'range',
                          'allowEmpty' => true,
                          'range'      => ['urWithoutSmr', 'bcRtk', 'networkModernization','withOutFilter'],
                          'error'      => '�������� �������� id �������',
                          'errorId'    => 'radio'],
            ],

            'page' => [['subject'   => $this->page,
                        'validator' => 'id',
                        'error'     => '�������� id ��������',
                        'errorId'   => 'page'],
            ],
        ];
    }

    public function getAllPlan()
    {
        $where = 'AND ur.plan_end_date BETWEEN "'.$this->dateFrom.'" AND "'.$this->dateTo.'" AND ur.end_date = "0000-00-00 00:00:00"';
        $where1 = 'AND ur.end_date BETWEEN "'.$this->dateFrom.'" AND "'.$this->dateTo.'" ';
        $order = 'objects.teoId';
        $orderFactDevice = null;
        $orderEbitdaRentSort = null;
        $orderPaybackSort = null;
        $orderFactEbitdaRentSort = null;
        $orderFactPaybackSort = null;
        $limit = 'LIMIT '.$this->start.','.$this->pageLen.'';
        if ($this->isFilter == 1)
        {

            if ($this->teoId != null or $this->teoId != '')
            {
                $where .= ' AND objects.teoId = "'.$this->teoId.'" ';
                $where1 .= ' AND objects.teoId = "'.$this->teoId.'" ';
            }

            if ($this->sellerId != null or $this->sellerId != '')
            {
                $where .= ' AND inv_sellers.id = "'.$this->sellerId.'" ';
                $where1 .= ' AND inv_sellers.id = "'.$this->sellerId.'" ';
            }

            if ($this->houseId != null or $this->houseId != '')
            {
                $where .= ' AND ur.houseId = "'.$this->houseId.'" ';
                $where1 .= ' AND ur.houseId = "'.$this->houseId.'" ';
            }

            if ($this->planDateFinishBuild != null or $this->planDateFinishBuild != '')
            {
                $where .= ' AND ur.plan_end_date = "'.$this->planDateFinishBuild.'" ';
                $where1 .= ' AND ur.plan_end_date = "'.$this->planDateFinishBuild.'" ';
            }

            if ($this->planDateInstallService != null or $this->planDateInstallService != '')
            {
                $where .= ' AND document.planServiceDate = "'.$this->planDateInstallService.'" ';
                $where1 .= ' AND document.planServiceDate = "'.$this->planDateInstallService.'" ';
            }

            if ($this->planDateLineDepartment != null or $this->planDateLineDepartment != '')
            {
                $where .= ' AND plan_connect.date_exit = "'.$this->planDateLineDepartment.'" ';
                $where1 .= ' AND plan_connect.date_exit = "'.$this->planDateLineDepartment.'" ';
            }

            if ($this->isFinishDate == 1 and $this->planDateFinishBuild == '')
            {
                $where .= ' AND ur.plan_end_date IS NULL';
                $where1 .= ' AND ur.plan_end_date IS NULL';
            }

            if ($this->isDateLineDepartment == 1 and $this->planDateLineDepartment == '')
            {
                $where .= ' AND plan_connect.date_exit IS NULL';
                $where1 .= ' AND plan_connect.date_exit IS NULL';
            }

            if ($this->teoProjectName != null or $this->teoProjectName != '')
            {
                $where .= ' AND teo_project.project_name LIKE "%'.$this->teoProjectName.'%" ';
                $where1 .= ' AND teo_project.project_name LIKE "%'.$this->teoProjectName.'%" ';
            }

            if ($this->manager != null or $this->manager != '')
            {
                $where1 .= ' AND ur.manager LIKE "%'.$this->manager.'%" ';
            }

            if ($this->projectManager != null or $this->projectManager != '')
            {
                $where .= ' AND ur.project_manager LIKE "%'.$this->projectManager.'%" ';
                $where1 .= ' AND ur.project_manager LIKE "%'.$this->projectManager.'%" ';
            }

            if ($this->isManager == 1 and $this->manager == '')
            {
                $where .= ' AND ur.manager = ""';
                $where1 .= ' AND ur.manager = ""';
            }

            if ($this->isProjectManager == 1 and $this->projectManager == '')
            {
                $where .= ' AND ur.project_manager = ""';
                $where1 .= ' AND ur.project_manager = ""';
            }

            if ($this->isTeoId == 1 and $this->teoProjectName == '')
            {
                $where .= ' AND objects.teoId = 0';
                $where1 .= ' AND objects.teoId = 0';
            }

            if ($this->isPlanDateInstallService == 1 and $this->planDateInstallService == '')
            {
                $where .= ' AND document.planServiceDate IS NULL';
            }

            if ($this->factSmrSort != null or $this->factSmrSort != '')
                $order = 'smr.fact_cost '.$this->factSmrSort.' ';

            if ($this->planSmrSort != null or $this->planSmrSort != '')
                $order = 'ur.teo_cost '.$this->planSmrSort.' ';

            if ($this->planDeviceSort != null or $this->planDeviceSort != '')
                $order = 'device_sum_address.cost '.$this->planDeviceSort.' ';

            if ($this->deltaSmrSort != null or $this->deltaSmrSort != '')
                $order = 'deltaSmr '.$this->deltaSmrSort.' ';

            if ($this->capexSort != null or $this->capexSort != '')
                $order = 'capex '.$this->capexSort.' ';

            if ($this->opexSort != null or $this->opexSort != '')
                $order = 'opex '.$this->opexSort.' ';

            if ($this->ebitdaRentSort != null or $this->ebitdaRentSort != '')
                $orderEbitdaRentSort = $this->ebitdaRentSort;

            if ($this->paybackSort != null or $this->paybackSort != '')
                $orderPaybackSort = $this->paybackSort;

            if ($this->factDeviceSort != null or $this->factDeviceSort != '')
                $orderFactDevice = $this->factDeviceSort;

            if ($this->factEbitdaRentSort != null or $this->factEbitdaRentSort != '')
                $orderFactEbitdaRentSort = $this->factEbitdaRentSort;

            if ($this->factPaybackSort != null or $this->factPaybackSort != '')
                $orderFactPaybackSort = $this->factPaybackSort;

            if ($this->bcRtk == 1)
            {
                $where .= ' AND objects.contractId = "109135"';
                $where1 .= ' AND objects.contractId = "109135"';
            }

            if ($this->networkModernization == 1)
            {
                $where .= ' AND clients.contract_no  IN (\'666/5\',\'666/2\',\'666/1\')';
                $where1 .= ' AND clients.contract_no  IN (\'666/5\',\'666/2\',\'666/1\')';
            }

            if ($this->withOutFilter == 1)
            {
                $where .= '';
                $where1 .= '';
            }

            if ($this->urWithoutSmr == 1)
            {
                $where = ' AND smr.invoice IS NULL AND ur.plan_end_date > "2015-01-01"';
                $where1 = ' AND smr.invoice IS NULL AND ur.plan_end_date > "2015-01-01"';
            }

        }

        if ($this->isCsv != null)
            $limit = '';

        $sql = 'SELECT 
                    teo_addr.id AS addrId,
                    REPLACE(REPLACE(IF(teo_project.project_name IS NULL, "�� ��������� ���", teo_project.project_name),\'&amp;\',\'&\'),\'&quot;\', \'"\') AS teoProjectName,
                    ur.task_id,
                    ur.task_id AS urId,
                    smr.id AS smrId,
                    objects.objectId,
                    objects.teoId,
                    objects.contractId as objectContractId, 
                    objects.houseId as objectHouseId, 
                    CONCAT_WS(", ",fst_address.city_name,fst_address.street_name,fst_address.house_name) AS address,
                    ur.houseId,
                    DATE_FORMAT(ur.plan_end_date, "%Y-%m-%d") AS planSmrDate,
                    IF (DATE_FORMAT(ur.end_date, "%Y-%m-%d") = "0000-00-00", "", DATE_FORMAT(ur.end_date, "%Y-%m-%d")) AS factSmrDate,
                    plan_connect.date_exit AS dateExit, 
                    plan_connect.plan_connect_id AS planConnectId,
                    ur.teo_cost AS planSmrCost,
                    COALESCE(smr.fact_cost, 0) AS factSmrCost,
                    COALESCE(device_sum_address.cost,0) AS planDeviceCost,
                    ur.manager, 
                    ur.project_manager AS projectManager,
                    REPLACE(inv_sellers.full_name, \'&quot;\',\'"\') as seller,
                    document.planServiceDate,
                    smr.date_invoice as dateInvoice,
                    smr.capex,
                    smr.opex,
                    smr.quantity,
                    (ur.teo_cost - COALESCE(smr.fact_cost, 0)) AS deltaSmr,
                    clients.contract_no AS contract,
                    clients.sphere,
                    REPLACE(clients.brand,\'&quot;\',\'"\') as brand,
                    (teo_content.e1_sum_ab+teo_content.int_sum_ab+teo_content.kpd_sum_ab+pp_em+teo_content.al_sum_ab) AS planSumIncome,
                    teo_content.cost_ac_em,
                    teo_content.cost_etc_em,
                    teo_content.cost_ap_em,
                    teo_content.cost_adi_em,
                    teo_content.cost_av_em,
                    teo_content.cost_av_ev,
                    teo_content.cost_ac_ev,
                    teo_content.cost_etc_ev,
                    teo_content.cost_ap_ev,
                    teo_content.cost_adi_ev,
                    teo_content.kpd_sum_ab,
                    teo_content.kpd,
                    teo_content.kpd_sum_connect,
                    teo_content.int,
                    teo_content.int_sum_connect,
                    teo_content.int_sum_ab,
                    teo_content.al,
                    teo_content.al_sps,
                    teo_content.al_sum_min,
                    teo_content.al_sum_ab,
                    teo_content.al_count,
                    teo_content.al_mg_mn,
                    teo_content.al_sum_connect,
                    teo_content.e1,
                    teo_content.e1_sps,
                    teo_content.e1_sum_min,
                    teo_content.e1_sum_ab,
                    teo_content.e1_count_lines,
                    teo_content.e1_mg_mn,
                    teo_content.e1_sum_connect,
                    teo_content.pp_ev,
                    teo_content.pp_em,
                    teo_content.tn_sum_connect     
                FROM 
                    '.MonCms::$config['db_cms_name'].'.ur_main AS ur
                LEFT JOIN
                    '.MonCms::$config['db_fst_name'].'.fst_address
                ON
                    ur.houseId = fst_address.house_id
                LEFT JOIN 
                    '.MonCms::$config['db_inventory_name'].'.smr    
                ON 
                    ur.task_id = smr.urId
                LEFT JOIN
                    '.MonCms::$config['db_fst_name'].'.objects
                ON
                    ur.obj_id = objects.objectId
                LEFT JOIN
                    '.MonCms::$config['db_fst_name'].'.clients
                ON
                    objects.contractId = clients.order_id
                LEFT JOIN
                    '.MonCms::$config['db_project_name'].'.teo_addr
                ON
                    fst_address.house_id = teo_addr.house_id
                    AND teo_addr.teo_id = objects.teoId
                LEFT JOIN
                    '.MonCms::$config['db_project_name'].'.document
                ON
                    objects.teoId = document.teoId
                LEFT JOIN
                        (
                        SELECT 
                            t1.addr_id, 
                            SUM(t1.cost) AS cost
                        FROM 
                            (
                                SELECT 
                                    addr_id, 
                                    add_cost AS cost
                                FROM 
                                    '.MonCms::$config['db_project_name'].'.teo_manual_costs
                                LEFT JOIN 
                                    '.MonCms::$config['db_project_name'].'.teo_addr 
                                ON 
                                    teo_manual_costs.addr_id = teo_addr.id
                                WHERE 
                                    teo_manual_costs.type_cost = 0 
                                UNION
                                SELECT 
                                    addr_id, 
                                    add_cost AS cost
                                FROM 
                                    '.MonCms::$config['db_project_name'].'.teo_addr_costs
                                LEFT JOIN 
                                    '.MonCms::$config['db_project_name'].'.teo_addr 
                                ON 
                                    teo_addr_costs.addr_id = teo_addr.id
                           ) AS t1
                        GROUP BY t1.addr_id
                    ) AS device_sum_address
                ON
                    teo_addr.id = device_sum_address.addr_id
                LEFT JOIN
                    '.MonCms::$config['db_project_name'].'.teo_project
                ON
                    objects.teoId = teo_project.id
                LEFT JOIN
                    '.MonCms::$config['db_project_name'].'.teo_addr as taddr
                ON
                    teo_project.id = taddr.teo_id
                    AND objects.houseId = taddr.house_id
                LEFT JOIN
                    '.MonCms::$config['db_project_name'].'.teo_content
                ON
                    taddr.id = teo_content.addr_id
                    
                LEFT JOIN
                    '.MonCms::$config['db_inventory_name'].'.inv_sellers
                ON
                    ur.seller = inv_sellers.id
                LEFT JOIN 
                    '.MonCms::$config['db_cms_name'].'.plan_connect 
                ON 
                    objects.houseId = plan_connect.house_id 
                    AND objects.contractId = plan_connect.contract_id
                LEFT JOIN 
                    '.MonCms::$config['db_cms_name'].'.plan_connect AS planConnect2 
                ON 
                    objects.houseId = planConnect2.house_id 
                    AND objects.contractId = planConnect2.contract_id 
                    AND plan_connect.date_exit < planConnect2.date_exit
                WHERE
                    ur.`status` NOT IN ("REJECT","PAUSED","PLAN")
                    AND ur.`taskTypeCode` != "CH_PASS"
                    AND planConnect2.date_exit IS NULL
                    '.$where1.'
                GROUP BY
                    ur.task_id
                ORDER BY
                    '.$order;
        $result['planCapital'] = MonCms::$db->fetchAllAssoc($sql);

        $sql = 'SELECT 
                    teo_addr.id AS addrId,
                    REPLACE(REPLACE(IF(teo_project.project_name IS NULL, "�� ��������� ���", teo_project.project_name),\'&amp;\',\'&\'),\'&quot;\', \'"\') AS teoProjectName,
                    ur.task_id,
                    ur.task_id AS urId,
                    smr.id AS smrId,
                    objects.objectId,
                    objects.teoId,
                    objects.contractId as objectContractId, 
                    objects.houseId as objectHouseId, 
                    CONCAT_WS(", ",fst_address.city_name,fst_address.street_name,fst_address.house_name) AS address,
                    ur.houseId,
                    DATE_FORMAT(ur.plan_end_date, "%Y-%m-%d") AS planSmrDate,
                    IF (DATE_FORMAT(ur.end_date, "%Y-%m-%d") = "0000-00-00", "", DATE_FORMAT(ur.end_date, "%Y-%m-%d")) AS factSmrDate,
                    plan_connect.date_exit AS dateExit, 
                    plan_connect.plan_connect_id AS planConnectId,
                    ur.teo_cost AS planSmrCost,
                    COALESCE(smr.fact_cost, 0) AS factSmrCost,
                    COALESCE(device_sum_address.cost,0) AS planDeviceCost,
                    ur.manager, 
                    ur.project_manager AS projectManager,
                    REPLACE(inv_sellers.full_name, \'&quot;\',\'"\') as seller,
                    document.planServiceDate,
                    smr.date_invoice as dateInvoice,
                    smr.capex,
                    smr.opex,
                    smr.quantity,
                    (ur.teo_cost - COALESCE(smr.fact_cost, 0)) AS deltaSmr,
                    clients.contract_no AS contract,
                    clients.sphere,
                    REPLACE(clients.brand,\'&quot;\',\'"\') as brand,
                    (teo_content.e1_sum_ab+teo_content.int_sum_ab+teo_content.kpd_sum_ab+pp_em+teo_content.al_sum_ab) AS planSumIncome,
                    teo_content.cost_ac_em,
                    teo_content.cost_etc_em,
                    teo_content.cost_ap_em,
                    teo_content.cost_adi_em,
                    teo_content.cost_av_em,
                    teo_content.cost_av_ev,
                    teo_content.cost_ac_ev,
                    teo_content.cost_etc_ev,
                    teo_content.cost_ap_ev,
                    teo_content.cost_adi_ev,
                    teo_content.kpd_sum_ab,
                    teo_content.kpd,
                    teo_content.kpd_sum_connect,
                    teo_content.int,
                    teo_content.int_sum_connect,
                    teo_content.int_sum_ab,
                    teo_content.al,
                    teo_content.al_sps,
                    teo_content.al_sum_min,
                    teo_content.al_sum_ab,
                    teo_content.al_count,
                    teo_content.al_mg_mn,
                    teo_content.al_sum_connect,
                    teo_content.e1,
                    teo_content.e1_sps,
                    teo_content.e1_sum_min,
                    teo_content.e1_sum_ab,
                    teo_content.e1_count_lines,
                    teo_content.e1_mg_mn,
                    teo_content.e1_sum_connect,
                    teo_content.pp_ev,
                    teo_content.pp_em,
                    teo_content.tn_sum_connect     
                FROM 
                    '.MonCms::$config['db_cms_name'].'.ur_main AS ur
                LEFT JOIN
                    '.MonCms::$config['db_fst_name'].'.fst_address
                ON
                    ur.houseId = fst_address.house_id
                LEFT JOIN 
                    '.MonCms::$config['db_inventory_name'].'.smr    
                ON 
                    ur.task_id = smr.urId
                LEFT JOIN
                    '.MonCms::$config['db_fst_name'].'.objects
                ON
                    ur.obj_id = objects.objectId
                LEFT JOIN
                    '.MonCms::$config['db_fst_name'].'.clients
                ON
                    objects.contractId = clients.order_id
                LEFT JOIN
                    '.MonCms::$config['db_project_name'].'.teo_addr
                ON
                    fst_address.house_id = teo_addr.house_id
                    AND teo_addr.teo_id = objects.teoId
                LEFT JOIN
                    '.MonCms::$config['db_project_name'].'.document
                ON
                    objects.teoId = document.teoId
                LEFT JOIN
                        (
                        SELECT 
                            t1.addr_id, 
                            SUM(t1.cost) AS cost
                        FROM 
                            (
                                SELECT 
                                    addr_id, 
                                    add_cost AS cost
                                FROM 
                                    '.MonCms::$config['db_project_name'].'.teo_manual_costs
                                LEFT JOIN 
                                    '.MonCms::$config['db_project_name'].'.teo_addr 
                                ON 
                                    teo_manual_costs.addr_id = teo_addr.id
                                WHERE 
                                    teo_manual_costs.type_cost = 0 
                                UNION
                                SELECT 
                                    addr_id, 
                                    add_cost AS cost
                                FROM 
                                    '.MonCms::$config['db_project_name'].'.teo_addr_costs
                                LEFT JOIN 
                                    '.MonCms::$config['db_project_name'].'.teo_addr 
                                ON 
                                    teo_addr_costs.addr_id = teo_addr.id
                           ) AS t1
                        GROUP BY t1.addr_id
                    ) AS device_sum_address
                ON
                    teo_addr.id = device_sum_address.addr_id
                LEFT JOIN
                    '.MonCms::$config['db_project_name'].'.teo_project
                ON
                    objects.teoId = teo_project.id
                LEFT JOIN
                    '.MonCms::$config['db_project_name'].'.teo_addr as taddr
                ON
                    teo_project.id = taddr.teo_id
                    AND objects.houseId = taddr.house_id
                LEFT JOIN
                    '.MonCms::$config['db_project_name'].'.teo_content
                ON
                    taddr.id = teo_content.addr_id
                    
                LEFT JOIN
                    '.MonCms::$config['db_inventory_name'].'.inv_sellers
                ON
                    ur.seller = inv_sellers.id
                LEFT JOIN 
                    '.MonCms::$config['db_cms_name'].'.plan_connect 
                ON 
                    objects.houseId = plan_connect.house_id 
                    AND objects.contractId = plan_connect.contract_id
                LEFT JOIN 
                    '.MonCms::$config['db_cms_name'].'.plan_connect AS planConnect2 
                ON 
                    objects.houseId = planConnect2.house_id 
                    AND objects.contractId = planConnect2.contract_id 
                    AND plan_connect.date_exit < planConnect2.date_exit
                WHERE
                    ur.`status` NOT IN ("REJECT","PAUSED","PLAN")
                    AND ur.`taskTypeCode` != "CH_PASS"
                    AND planConnect2.date_exit IS NULL
                    '.$where.'
                GROUP BY
                    ur.task_id
                ORDER BY
                    '.$order;

        $result['planCapital'] = array_merge($result['planCapital'], MonCms::$db->fetchAllAssoc($sql));

        $sql = 'SELECT
                    movements.invId,
                    movements.userId,
                    movements.objectId,
                    objects.houseId as serviceHouseId,
                    objects.teoId,
                    movements.currentUserId,
                    movements.date,
                    movements.typeBase,
                    IF(movements.typeBase = "os",
                    (
                        SELECT
                            inv_os.cost
                        FROM
                            inventory.inv_os
                        WHERE
                            movements.invId = inv_os.id
                    ),
                    0
                    ) AS osCost,
                    (inv_tmc.cost*inv_tmc.qty)+COALESCE(t.tmcCost,0) as tmcCost
                FROM
                    '.MonCms::$config['db_inventory_name'].'.movements
                LEFT JOIN
                (
                    SELECT
                        inventoryWrite.ownerId,
                        SUM(inv_tmc.cost * inventoryWrite.quantity) as tmcCost
                    FROM
                        '.MonCms::$config['db_inventory_name'].'.inventoryWrite
                    LEFT JOIN
                        '.MonCms::$config['db_inventory_name'].'.inv_tmc
                    ON
                        inventoryWrite.inventoryTmcId = inv_tmc.id
                    WHERE
                        inventoryWrite.inventoryTmcId IS NOT NULL
                        AND inventoryWrite.forWhat = "inventory"
                        AND inventoryWrite.date BETWEEN "'.$this->dateFrom.'" AND "'.$this->dateTo.'"
                    GROUP BY
                        inventoryWrite.ownerId
                    ) AS t
                ON
                    movements.invId = t.ownerId
                    AND movements.typeBase = "os"
                LEFT JOIN
                    '.MonCms::$config['db_inventory_name'].'.inv_tmc
                ON
                    movements.invId = inv_tmc.id
                    AND movements.typeBase = "tmc"
                    AND inv_tmc.id not in
                    (
                        SELECT DISTINCT
                            inventoryWrite.inventoryTmcId
                        FROM
                            '.MonCms::$config['db_inventory_name'].'.inventoryWrite
                        WHERE
                            inventoryWrite.inventoryTmcId IS NOT NULL
                            AND inventoryWrite.forWhat = "inventory"
                            AND inventoryWrite.date BETWEEN "'.$this->dateFrom.'" AND "'.$this->dateTo.'"
                    )
                LEFT JOIN
                    '.MonCms::$config['db_fst_name'].'.objects
                ON
                    movements.objectId = objects.objectId
                LEFT JOIN
                    '.MonCms::$config['db_project_name'].'.teo_project
                ON
                    objects.teoId = teo_project.id
                LEFT JOIN
                      '.MonCms::$config['db_inventory_name'].'.movements as movements1
                ON
                      movements.invId = movements1.invId
                      AND movements.date < movements1.date
                      AND movements1.date BETWEEN "'.$this->dateFrom.'" AND "'.$this->dateTo.'"
                WHERE
                    movements.invId IN
                    (
                        SELECT DISTINCT
                            movements.invId
                        FROM
                            project.teo_project
                        LEFT JOIN
                            fastcom.objects
                        ON
                            teo_project.id = objects.teoId
                        LEFT JOIN
                            inventory.movements
                        ON
                            objects.objectId = movements.objectId
                        WHERE
                            teo_project.teo_status = 1
                            AND movements.invId != 0 AND movements.invId IS NOT NULL
                            AND movements.date BETWEEN "'.$this->dateFrom.'" AND "'.$this->dateTo.'"
                    )
                    AND movements.date BETWEEN "'.$this->dateFrom.'" AND "'.$this->dateTo.'"
                    AND movements1.date IS NULL
                    AND objects.teoId != 0
                    AND teo_project.teo_status = 1';

        $result['factDeviceCost'] = MonCms::$db->fetchAllAssoc($sql);

        // ����������� teoId � ������ � ���� ������
        $tmp = [];
        $averageCapital = [];
        foreach ($result['planCapital'] as $value)
        {
            if($value['objectHouseId'] != '0' and $value['objectHouseId'] != null and $averageCapital['contractId'] != '0' and $value['objectContractId'] != null)
            {
                $averageCapital['houseId'][] = $value['objectHouseId'];
                $averageCapital['contractId'][] = $value['objectContractId'];
            }
            if ($value['teoId'] != '0' and $value['teoId'] != null)
                $tmp[] = $value['teoId'];
        }

        $tmp = implode(",", $tmp);
        $averageCapital['houseId'] = implode(",", array_unique($averageCapital['houseId']));
        $averageCapital['contractId'] = implode(",", array_unique($averageCapital['contractId']));

         if (!empty($tmp))
         {
            // ����������� ��������� ��� ����������� ���
            $sql = 'SELECT
                        teo_id as teoId,
                        const_id as constId,
                        value
                    FROM
                        '.MonCms::$config['db_project_name'].'.teo_proj_const
                    WHERE
                        teo_id IN ('.$tmp.')
                        AND const_id IN (3,4,5,10,11,12,17)';

            $teoProjConst = MonCms::$db->fetchAllAssoc($sql);

            // ������� ��� ������� ��O ���������� ��� �������� �����������
            $tmp = [];
            foreach ($teoProjConst as $key => $value)
                $tmp[$value['teoId']][$value['constId']] = $value['value'];
         }

         if ($averageCapital['houseId'] != null and $averageCapital['contractId'] != null)
         {
             $sql = 'SELECT
                          house_id,
                          min(date) as startPayDate,
                          contract_id,
                          avg(`sum`) as avgSum
                     FROM
                          '.MonCms::$config['db_fst_name'].'.average_capital
                     WHERE 
                          house_id IN ('.$averageCapital['houseId'].')
                          AND contract_id IN ('.$averageCapital['contractId'].')
                     GROUP BY
                          house_id,
                          contract_id';

            $averageCapital = MonCms::$db->fetchAllAssoc($sql);
         }

        $data = [];
        // �������� � ������� ���� ������� ����� ������������ ������� �� ���������� ������ ����������� �������
        foreach ($result['planCapital'] as $key => $value)
         {
             foreach ($averageCapital as $item) {
                 if ($value['objectContractId'] == $item['contract_id'] and $value['objectHouseId'] == $item['house_id'])
                 {
                     $result['planCapital'][$key]['averageSum'] = $item['avgSum'];
                     $result['planCapital'][$key]['startPayDate'] = $item['startPayDate'];
                     break;
                 }
                 else
                 {
                     $result['planCapital'][$key]['averageSum'] = 0;
                     $result['planCapital'][$key]['startPayDate'] = null;
                 }
             }
         }

        // �������� � ������� ���� ����������� ��������� ������������
        foreach ($result['planCapital'] as $key => $value)
        {
            $result['totalCost']['totalPlanSmrCost'] += $value['planSmrCost'];
            $result['totalCost']['totalFactSmrCost'] += $value['factSmrCost'];
            $result['totalCost']['totalPlanDeviceCost'] += $value['planDeviceCost'];
            $result['totalCost']['totalDeltaSmrCost'] += ($value['planSmrCost'] - $value['factSmrCost']);
            $result['totalCost']['capex'] += $value['capex'];
            $result['totalCost']['opex'] += $value['opex'];
            $result['planCapital'][$key]['factDeviceCost'] = 0;
            foreach ($result['factDeviceCost'] as $val)
            {
                if ($value['houseId'] == $val['serviceHouseId'])
                {
                    $result['totalCost']['totalFactDeviceCost'] += ($val['osCost'] + $val['tmcCost']);
                    $result['planCapital'][$key]['factDeviceCost'] += ($val['osCost'] + $val['tmcCost']);
                }

                $data['phone_ev'] = 0 + $value['tn_sum_connect'] + $value['al_sum_connect'] + $value['e1_sum_connect']; // �����������
                $data['phone_em'] = 0 + $value['al_sum_ab'] + $value['e1_sum_ab']; // ���������
                $data['phone_em_r'] = 0;
                $data['int_em_r'] = 0;
                $data['kpd_em_r'] = 0;
                $data['pr_em_r'] = 0;
                $data['income_em_r'] = 0;

                // ���� � ��� ���� al, ��������� �������� ���� ������� ������� �������� �� ����������� ������ + �������
                if ($value['al'] == 1)
                {
                    // ������� �� ���
                    if ($value['al_sps'] > 0)
                    {
                        $data['phone_em'] += $value['al_sps'];
                    }
                    else
                    {
                        if ($value['al_sum_min'] > 0)
                        {
                            $data['phone_em_r'] += ($value['al_count'] * $tmp[$value['teoId']][3] * $value['al_sum_min']);
                        }
                        else
                        {
                            $data['phone_em_r'] += ($value['al_count'] * $tmp[$value['teoId']][3] * $tmp[$value['teoId']][10]);
                        }
                    }
                    // ������� �� ��/��
                    if ($value['al_mg_mn'] > 0)
                    {
                        $data['phone_em'] += $value['al_mg_mn'];
                    }
                    else
                    {
                        $data['phone_em_r'] += ($value['al_count'] * $tmp[$value['teoId']][4] * $tmp[$value['teoId']][11]);
                    }

                    // ���������� ������
                    $data['phone_em_r'] += ($value['al_count'] * $tmp[$value['teoId']][5] * $tmp[$value['teoId']][12]);
                }


                // ���� � ��� ���� e1, ��������� �������� ���� ������� ������� �������� �� �����������
                if ($value['e1'] == 1)
                {
                    // ������� �� ���
                    if ($value['e1_sps'] > 0)
                    {
                        $data['phone_em'] += $value['e1_sps'];
                    }
                    else
                    {
                        if ($value['e1_sum_min'] > 0)
                        {
                            $data['phone_em_r'] += ($value['e1_count_lines'] * $tmp[$value['teoId']][3] * $value['e1_sum_min']);
                        }
                        else
                        {
                            $data['phone_em_r'] += ($value['e1_count_lines'] * $tmp[$value['teoId']][3] * $tmp[$value['teoId']][10]);
                        }
                    }

                    // ������� �� ��/��
                    if ($value['e1_mg_mn'] > 0)
                    {
                        $data['phone_em'] += $value['e1_mg_mn'];
                    }
                    else
                    {
                        $data['phone_em_r'] += ($value['e1_count_lines'] * $tmp[$value['teoId']][4] * $tmp[$value['teoId']][11]);
                    }
                    // ���������� ������
                    $data['phone_em_r'] += ($value['e1_count_lines'] * $tmp[$value['teoId']][5] * $tmp[$value['teoId']][12]);
                }

                $data['int_ev'] = 0 + $value['int_sum_connect'];
                $data['int_em'] = 0 + $value['int_sum_ab'];
                $data['kpd_ev'] = 0 + $value['kpd_sum_connect'];
                $data['kpd_em'] = 0 + $value['kpd_sum_ab'];
                $data['pr_ev'] = 0 + $value['pp_ev'];
                $data['pr_em'] = 0 + $value['pp_em'];
                $data['income_ev'] = 0 + $data['phone_ev'] + $data['int_ev'] + $data['kpd_ev'] + $data['pr_ev'];
                $data['income_em'] = 0 + $data['phone_em'] + $data['int_em'] + $data['kpd_em'] + $data['pr_em'];

                // �������

                $data['ag_ev'] = round(($data['income_ev'] / 100 * $value['cost_av_ev']), 2);
                $data['ag_em'] = round(($data['income_em'] / 100 * $value['cost_av_em']), 2);

                $outlay_ev = 0 + $value['cost_ac_ev'] + $value['cost_etc_ev'] + $value['cost_ap_ev'] + $data['ag_ev'] + $value['cost_adi_ev'];
                $outlay_em = 0 + $value['cost_ac_em'] + $value['cost_etc_em'] + $value['cost_ap_em'] + $data['ag_em'] + $value['cost_adi_em'];
                $incomePlan = 0 + round((($data['income_em'] + ($data['income_ev'] / 36)) - ($outlay_em + ($outlay_ev / 36) + $value['planDeviceCost'] /36)), 2);
                $incomeFact = 0 + round(($value['averageSum'] - ($outlay_em + ($outlay_ev / 36) + $value['factDeviceCost'] / 36)), 2);

                $result['planCapital'][$key]['outlayEv'] = $outlay_ev;
                $result['planCapital'][$key]['outlayEm'] = $outlay_em;
                $result['planCapital'][$key]['outlayAll'] = round(($outlay_em + ($outlay_ev / 36)), 2);
                $result['planCapital'][$key]['incomeAll'] = round(($data['income_em'] + ($data['income_ev'] / 36)), 2);

                //$incomePlan = ($data['income_em'] + ($data['income_ev'] / 36));
                if ($incomePlan != 0 and $incomePlan > 0)
                {
                    // ����������� EBITDA
                    $result['planCapital'][$key]['planEbitdaRent'] = round(($incomePlan / ($data['income_em'] + ($data['income_ev'] / 36))) * 100, 2);

                    // ��������� �����, ����� ��������� �����. ����������� ���� �����������
                    $result['planCapital'][$key]['planPayback'] = ceil(($value['planSmrCost'] + $value['planDeviceCost']) / ($data['income_em'] + (($data['income_ev'] / 36) - ($outlay_em + ($outlay_ev / 36)))));

                    if ($result['planCapital'][$key]['planPayback'] < 0)
                        $result['planCapital'][$key]['planPayback'] = '�� ���������';
                }
                else
                {
                    $result['planCapital'][$key]['planEbitdaRent'] = '- &infin; %';

                    $result['planCapital'][$key]['planPayback'] = '�� ���������';

                }

                if($value['averageSum'] != 0)
                {
                    // ����������� EBITDA
                    $result['planCapital'][$key]['factEbitdaRent'] = round(($incomeFact / $result['planCapital'][$key]['averageSum']) * 100, 2);

                    // ��������� �����, ����� ��������� �����. ����������� ���� �����������
                    $result['planCapital'][$key]['factPayback'] = ceil(($value['factSmrCost'] + $value['factDeviceCost']) / ($value['averageSum'] - ($outlay_em + ($outlay_ev / 36))));

                    if ($result['planCapital'][$key]['factPayback'] < 0)
                        $result['planCapital'][$key]['factPayback'] = '�� ���������';
                }
                else
                {
                    $result['planCapital'][$key]['factEbitdaRent'] = '- &infin; %';
                    $result['planCapital'][$key]['factPayback'] = '�� ���������';
                }
            }
        }

        // ���������� �� ������, ������� ���������� �� ���� � �������.
        if (!$orderFactDevice == null) // ���������� �� ����������� ��������� ������������
        {
            foreach ($result['planCapital'] as $key => $val)
            {
                $cost[$key] = $val['factDeviceCost'];
            }

            if ($orderFactDevice == 'asc')
            {
                array_multisort($cost, SORT_ASC, $result['planCapital']);
            }
            elseif ($orderFactDevice == 'desc')
            {
                array_multisort($cost, SORT_DESC, $result['planCapital']);
            }
        }
        elseif (!$orderEbitdaRentSort == null) // ���������� �� ����������� �������������� EBITDA
        {
            foreach ($result['planCapital'] as $key => $val)
            {
                $cost[$key] = $val['planEbitdaRent'];
            }

            if ($orderEbitdaRentSort == 'asc')
            {
                array_multisort($cost, SORT_ASC, $result['planCapital']);
            }
            elseif ($orderEbitdaRentSort == 'desc')
            {
                array_multisort($cost, SORT_DESC, $result['planCapital']);
            }
        }
        elseif (!$orderPaybackSort == null) // ���������� �� ������������ ����� �����������
        {
            foreach ($result['planCapital'] as $key => $val)
            {
                $cost[$key] = $val['planPayback'];
            }

            if ($orderPaybackSort == 'asc')
            {
                array_multisort($cost, SORT_ASC, $result['planCapital']);
            }
            elseif ($orderPaybackSort == 'desc')
            {
                array_multisort($cost, SORT_DESC, $result['planCapital']);
            }
        }
        elseif (!$orderFactEbitdaRentSort == null) // ���������� �� ����������� �������������� EBITDA
        {
            foreach ($result['planCapital'] as $key => $val)
            {
                $cost[$key] = $val['factEbitdaRent'];
            }

            if ($orderFactEbitdaRentSort == 'asc')
            {
                array_multisort($cost, SORT_ASC, $result['planCapital']);
            }
            elseif ($orderFactEbitdaRentSort == 'desc')
            {
                array_multisort($cost, SORT_DESC, $result['planCapital']);
            }
        }
        elseif (!$orderFactPaybackSort == null) // ���������� �� ������������ ����� �����������
        {
            foreach ($result['planCapital'] as $key => $val)
            {
                $cost[$key] = $val['factPayback'];
            }

            if ($orderFactPaybackSort == 'asc')
            {
                array_multisort($cost, SORT_ASC, $result['planCapital']);
            }
            elseif ($orderFactPaybackSort == 'desc')
            {
                array_multisort($cost, SORT_DESC, $result['planCapital']);
            }
        }

        return $result;
    }

    public function countPlan()
    {
        $where = '';
        if ($this->isFilter == 1)
        {
            if (!$this->teoId == null or !$this->teoId == '')
                $where .= ' AND objects.teoId = "'.$this->teoId.'" ';

            if (!$this->houseId == null or !$this->houseId == '')
                $where .= ' AND ur.houseId = "'.$this->houseId.'" ';

            if (!$this->planDateFinishBuild == null or !$this->planDateFinishBuild == '')
                $where .= ' AND ur.plan_end_date = "'.$this->planDateFinishBuild.'" ';

            if (!$this->planDateInstallService == null or !$this->planDateInstallService == '')
                $where .= ' AND document.planServiceDate = "'.$this->planDateInstallService.'" ';

            if (!$this->planDateLineDepartment == null or !$this->planDateLineDepartment == '')
                $where .= ' AND plan_connect.date_exit = "'.$this->planDateLineDepartment.'" ';

            if ($this->isFinishDate == 1 and $this->planDateFinishBuild == '')
                $where .= ' AND ur.plan_end_date IS NULL';

            if ($this->isDateLineDepartment == 1 and $this->planDateLineDepartment == '')
                $where .= ' AND plan_connect.date_exit IS NULL';

            if (!$this->teoProjectName == null or !$this->teoProjectName == '')
                $where .= ' AND teo_project.project_name LIKE "%'.$this->teoProjectName.'%" ';

            if (!$this->manager == null or !$this->manager == '')
                $where .= ' AND ur.manager LIKE "%'.$this->manager.'%" ';

            if (!$this->projectManager == null or !$this->projectManager == '')
                $where .= ' AND ur.project_manager LIKE "%'.$this->projectManager.'%" ';

            if ($this->isManager == 1 and $this->manager == '')
                $where .= ' AND ur.manager = ""';

            if ($this->isProjectManager == 1 and $this->projectManager == '')
                $where .= ' AND ur.project_manager = ""';

            if ($this->isTeoId == 1 and $this->teoProjectName == '')
                $where .= ' AND objects.teoId = 0';

            if ($this->isPlanDateInstallService == 1 and $this->planDateInstallService == '')
                $where .= ' AND document.planServiceDate IS NULL';
        }

        $sql = 'SELECT
                    count(t.task_id) as countPlan
                FROM
                    (SELECT 
                        ur.task_id
                    FROM 
                        '.MonCms::$config['db_cms_name'].'.ur_main AS ur
                    LEFT JOIN
                        '.MonCms::$config['db_fst_name'].'.fst_address
                    ON
                        ur.houseId = fst_address.house_id
                    LEFT JOIN 
                        '.MonCms::$config['db_inventory_name'].'.smr 
                    ON 
                        ur.task_id = smr.urId
                    LEFT JOIN
                        '.MonCms::$config['db_fst_name'].'.objects
                    ON
                        ur.obj_id = objects.objectId
                    LEFT JOIN
                        '.MonCms::$config['db_project_name'].'.teo_addr
                    ON
                        fst_address.house_id = teo_addr.house_id
                        AND teo_addr.teo_id = objects.teoId
                    LEFT JOIN
                        '.MonCms::$config['db_project_name'].'.document
                    ON
                        objects.teoId = document.teoId
                    LEFT JOIN
                        '.MonCms::$config['db_project_name'].'.teo_project
                    ON
                        objects.teoId = teo_project.id
                    LEFT JOIN
                        '.MonCms::$config['db_inventory_name'].'.inv_sellers
                    ON
                        ur.seller = inv_sellers.id
                    LEFT JOIN 
                        '.MonCms::$config['db_cms_name'].'.plan_connect 
                    ON 
                        objects.houseId = plan_connect.house_id 
                        AND objects.contractId = plan_connect.contract_id
                    LEFT JOIN 
                        '.MonCms::$config['db_cms_name'].'.plan_connect AS planConnect2 
                    ON 
                        objects.houseId = planConnect2.house_id 
                        AND objects.contractId = planConnect2.contract_id 
                        AND plan_connect.date_exit < planConnect2.date_exit
                    WHERE
                        ur.plan_end_date BETWEEN "'.$this->dateFrom.'" AND "'.$this->dateTo.'" 
                        AND ur.`status` NOT IN ("REJECT","PAUSED","PLAN")
                        AND ur.`taskTypeCode` != "CH_PASS"
                        '.$where.'
                    GROUP BY
                        ur.task_id
                    ORDER BY
                        objects.teoId) as t';

        $result = MonCms::$db->fetchRow($sql);

        return $result['countPlan'];
    }

}