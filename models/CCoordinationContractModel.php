<?php
namespace models;

use system\AModel;
use system\CVarDump;
use system\MonCms;

/**
 * Created by PhpStorm.
 * User: malyshevis
 * Date: 07.02.17
 * Time: 14:02
 */
class CCoordinationContractModel extends AModel
{
    CONST prototypeStatus = 1; // ������: ��������
    CONST inCoordinationStatus = 2; // ������: �� ������������
    CONST coordinateStatus = 3; // ������: ����������
    CONST rejectStatus = 4; // ������: ��������
    CONST contract = 1; // ��� ���������: �������
    CONST additionalAgreement = 2;  // ��� ���������: ���.����������
    CONST cancellation = 3;  // ��� ���������: �����������


    public $brandId;
    public $contractNo;
    public $clientId;
    public $coordinationId;
    public $userId;
    public $uploadFileCoordinationDescription;
    public $typeDocumentId;
    public $lastTypeDocumentId;
    public $orderId;
    public $fileId;
    public $followerUserId;
    public $statusId;
    public $lastStatusId;
    public $parentId = 0;
    public $commentId = 0;
    public $commentText;
    public $answerCommentText = '';
    public $answerCommentUserName = '';
    public $answerCommentAddDate = '';
    public $lastTypeDocumentDescription = '';
    public $lastStatusDescription = '';

    public function rules()
    {
        return [
            'brandId' => [['subject'   => $this->brandId,
                           'validator' => 'regexp',
                           'pattern'   => '/^(?!0)\d{1,6}$/',
                           'error'     => '�������� id ������',
                           'errorId'   => 'brandId']],

            'clientId' => [['subject'   => $this->clientId,
                            'validator' => 'regexp',
                            'pattern'   => '/^(?!0)\d{1,6}$/',
                            'error'     => '�������� id �������',
                            'errorId'   => 'clientId']],

            'userId' => [['subject'   => $this->userId,
                          'validator' => 'regexp',
                          'pattern'   => '/^[1-9][[:digit:]]*$/',
                          'error'     => '�������� id ������������',
                          'errorId'   => 'userId']],

            'coordinationId' => [['subject'   => $this->coordinationId,
                                  'validator' => 'regexp',
                                  'pattern'   => '/^[1-9][[:digit:]]*$/',
                                  'error'     => '�������� id ������������',
                                  'errorId'   => 'coordinationId']],

            'coordinationFile' => [['subject'   => $this->brandId,
                                    'validator' => 'regexp',
                                    'pattern'   => '/^(?!0)\d{1,6}$/',
                                    'error'     => '�������� id ������',
                                    'errorId'   => 'brandId'],

                                   ['subject'   => $this->clientId,
                                    'validator' => 'regexp',
                                    'pattern'   => '/^(?!0)\d{1,6}$/',
                                    'error'     => '�������� id �������',
                                    'errorId'   => 'clientId'],

                                   ['subject'   => $this->userId,
                                    'validator' => 'regexp',
                                    'pattern'   => '/^[1-9][[:digit:]]*$/',
                                    'error'     => '�������� id ������������',
                                    'errorId'   => 'userId'],

                                   ['subject'   => $this->coordinationId,
                                    'validator' => 'regexp',
                                    'pattern'   => '/^[1-9][[:digit:]]*$/',
                                    'error'     => '�������� id ������������',
                                    'errorId'   => 'coordinationId']],

            'createCoordination' => [['subject'   => $this->brandId,
                                      'validator' => 'regexp',
                                      'pattern'   => '/^(?!0)\d{1,6}$/',
                                      'error'     => '�������� id ������',
                                      'errorId'   => 'brandId'],

                                     ['subject'   => $this->clientId,
                                      'validator' => 'regexp',
                                      'pattern'   => '/^(?!0)\d{1,6}$/',
                                      'error'     => '�������� id �������',
                                      'errorId'   => 'clientId'],

                                     ['subject'   => $this->typeDocumentId,
                                      'validator' => 'id',
                                      'error'     => '�������� id ���� ���������',
                                      'errorId'   => 'typeDocumentId'],

                                     ['subject'   => $this->orderId,
                                      'validator' => 'id',
                                      'error'     => '�������� id ��������',
                                      'errorId'   => 'orderId']],

            'saveCoordination' => [['subject'   => $this->coordinationId,
                                    'validator' => 'regexp',
                                    'pattern'   => '/^[1-9][[:digit:]]*$/',
                                    'error'     => '�������� id ������������',
                                    'errorId'   => 'coordinationId'],

                                   ['subject'   => $this->typeDocumentId,
                                    'validator' => 'id',
                                    'error'     => '�������� id ���� ���������',
                                    'errorId'   => 'typeDocumentId'],

                                   ['subject'   => $this->statusId,
                                    'validator' => 'id',
                                    'error'     => '�������� id �������',
                                    'errorId'   => 'statusId'],

                                   ['subject'   => $this->lastTypeDocumentId,
                                    'validator' => 'id',
                                    'error'     => '�������� id ���� ���������',
                                    'errorId'   => 'lastTypeDocumentId'],

                                   ['subject'   => $this->lastStatusId,
                                    'validator' => 'id',
                                    'error'     => '�������� id �������',
                                    'errorId'   => 'lastStatusId']],

            'comment' => [['subject'   => $this->coordinationId,
                           'validator' => 'regexp',
                           'pattern'   => '/^[1-9][[:digit:]]*$/',
                           'error'     => '�������� id ������������',
                           'errorId'   => 'coordinationId'],

                          ['subject'   => $this->parentId,
                           'validator' => 'regexp',
                           'pattern'   => '/^[0]|[1-9][[:digit:]]*$/',
                           'error'     => '�������� id ��������',
                           'errorId'   => 'parentId'],

                          ['subject'   => $this->commentId,
                           'validator' => 'regexp',
                           'pattern'   => '/^[0]|[1-9][[:digit:]]*$/',
                           'error'     => '�������� id �����������',
                           'errorId'   => 'commentId']],

            'followerUserId' => [['subject'   => $this->followerUserId,
                                  'validator' => 'id',
                                  'error'     => '�������� id ������������',
                                  'errorId'   => 'followerUserId']],

            'fileId' => [['subject'   => $this->fileId,
                          'validator' => 'id',
                          'error'     => '�������� id �����',
                          'errorId'   => 'fileId']],
        ];
    }


    /**
     * ����������� ���� �� �������� ������������
     * @return mixed
     */
    public function viewCoordination()
    {
        $sql = 'SELECT
                    client_coordination.id,
                    client_coordination.client_id as clientId,
                    client_coordination.user_id as userId,
                    client_coordination.add_date as addDate,
                    client_coordination.brand_id as brandId,
                    client_coordination.order_id as orderId,
                    client_coordination.type_document_id as typeDocumentId,
                    client_coordination.status_id as statusId,
                    u.fullname,
                    client_coordination_status.coordination_status as status,
                    fstClients.fullname as clientName,
                    fstClients.contract_no as contractNo
                FROM
                    '.MonCms::$config['db_project_name'].'.client_coordination
                LEFT JOIN
                    '.MonCms::$config['db_cms_name'].'.users2 as u
                ON
                    client_coordination.user_id = u.user_id
                LEFT JOIN
                    '.MonCms::$config['db_project_name'].'.client_coordination_status
                ON
                    client_coordination.status_id = client_coordination_status.id
                LEFT JOIN
                    '.MonCms::$config['db_fst_name'].'.clients as fstClients
                ON
                    client_coordination.order_id = fstClients.order_id
                WHERE
                    client_coordination.id = '.$this->coordinationId.'';

        $result['clientCoordination'] = MonCms::$db->fetchRow($sql);

        $sql = 'SELECT
                    file.id,
                    file.user_id as userId,
                    file.add_date as addDate,
                    file.file_name as fileName,
                    file.description,
                    u.fullname
               FROM
                    '.MonCms::$config['db_project_name'].'.client_coordination_files as file
               LEFT JOIN
                    '.MonCms::$config['db_cms_name'].'.users2 as u
               ON
                    file.user_id = u.user_id
               WHERE
                    file.client_coordination_id = '.$this->coordinationId.'
               ORDER BY
                    file.add_date DESC';

        $result['clientCoordinationFile'] = MonCms::$db->fetchAllAssoc($sql);

        return $result;

    }


    /**
     * ���������� ��������� ������� ��� ���� ���������
     */
    public function saveCoordination()
    {
        $commentText = '';

        if ($this->typeDocumentId != $this->lastTypeDocumentId)
            $commentText .= '��� ��������� �� ���������: '.$this->lastTypeDocumentDescription.'<br>';

        if ($this->statusId != $this->lastStatusId)
            $commentText .= '������ ������������ �� ���������: '.$this->lastStatusDescription.'<br>';

        $sql = 'UPDATE
                    '.MonCms::$config['db_project_name'].'.client_coordination
                SET
                    type_document_id = '.$this->typeDocumentId.',
                    status_id = '.$this->statusId.'
                WHERE
                    id = '.$this->coordinationId.'';

        MonCms::$db->query($sql);

        $this->addComment($commentText);

        // ���� ������� ������ �������� � ����� ���������� ���������� ������������ ��������� � ������ ������������, �� ���������� �� ����� ���������� ����������
        if ($this->lastStatusId == self::prototypeStatus)
        {
            $title = '������������ �������� � '.$this->contractNo;
            $msg = '������� ����� ������������ ��� �������� � <a href="'.MonCms::$config['http_home'].'index.php?mod=coordinationContract&coordinationId='.$this->coordinationId.'">'.$this->contractNo.'</a>.<br/>';
            $msg .= $commentText;

            $this->sendNotification($title, $msg);
        }
        else
        {
            $title = '������������ �������� � '.$this->contractNo;
            $msg = '��������� ��������� � ������������ ��� �������� � <a href="'.MonCms::$config['http_home'].'index.php?mod=coordinationContract&coordinationId='.$this->coordinationId.'">'.$this->contractNo.'</a>.<br/>';
            $msg .= $commentText;

            $this->sendNotification($title, $msg);
        }
    }


    /**
     * ����������� ���� ����������
     * @return array|null
     */
    public function getDocumentType()
    {
        $sql = 'SELECT
                    id,
                    document_type as documentType
                FROM
                    '.MonCms::$config['db_project_name'].'.client_coordination_type';

        $result = MonCms::$db->fetchAllAssoc($sql);

        return $result;
    }

    public function getCoordinationInfo($coordinationId = 1)
    {
         $sql = 'SELECT
                    follower.coordination_file_id as coordinationFileId,
                    `c`.status_id as statusId,
                    clients.contract_no
                FROM
                    '.MonCms::$config['db_project_name'].'.`client_coordination_follower` as follower
                LEFT JOIN
                    '.MonCms::$config['db_project_name'].'.client_coordination as c
                ON
                    follower.client_coordination_id = c.id
                LEFT JOIN
                    fastcom.clients
                ON
                    c.order_id = clients.order_id
                WHERE
                    `client_coordination_id` = '.$coordinationId.'';

        $result = MonCms::$db->fetchRow($sql);

        return $result;
    }
    /**
     * ����������� ��� ����������� ��������, ������������� � ������
     * @return array|null
     */
    public function getOrderInfo()
    {
        $sql = 'SELECT
                    contract_no as contractNo,
                    order_id as orderId,
                    fullname as clientName
                FROM
                    '.MonCms::$config['db_fst_name'].'.`clients`
                WHERE
                    brandId = '.$this->brandId.'
                    AND cancel_date IS NULL';

        $result = MonCms::$db->fetchAllAssoc($sql);

        return $result;
    }


    /**
     * ������� ����� ������������
     */
    public function createNewCoordination()
    {

        $sql = 'INSERT INTO
                   '.MonCms::$config['db_project_name'].'.`client_coordination`
                SET
                    `client_id` = '.$this->clientId.',
                    `user_id` = '.MonCms::$user->userId.',
                    `add_date` = NOW(),
                    `brand_id` = '.$this->brandId.',
                    `order_id` = '.$this->orderId.',
                    `type_document_id` = '.$this->typeDocumentId.',
                    `status_id` = 1';
        MonCms::$db->query($sql);

        $lastId = MonCms::$db->lastInsertId();

        //����� ��������� ���� � ������������
        $this->coordinationId = $lastId;
        $this->followerUserId = MonCms::$user->userId;
        $this->addFollower();

        return $lastId;
    }

    /**
     * ��������� ������ ��������� ������������
     */
    public function addFollower()
    {
        $coordinationInfo = $this->getCoordinationInfo($this->coordinationId);

        //���� ���������� ���� �� ���� ����
        if (count($coordinationInfo) > 0 and $coordinationInfo['coordinationFileId'] != null)
        {
            //� ���� ������ �� ��������, �� ��������� ����������
            if ($coordinationInfo['statusId'] != self::prototypeStatus)
            {
                $sql = 'SELECT
                            fullname
                        FROM
                            '.MonCms::$config['db_cms_name'].'.`users2`
                        WHERE
                            user_id = '.$this->followerUserId;

                $result = MonCms::$db->fetchRow($sql);

                $title = '������������ �������� � '.$coordinationInfo['contract_no'];
                $msg = '�������� ����� ������� ������������ '.$result['fullname'].' ��� �������� � <a href="'.MonCms::$config['http_home'].'index.php?mod=coordinationContract&coordinationId='.$this->coordinationId.'">'.$coordinationInfo['contract_no'].'</a>.';

                $this->sendNotification($title, $msg);

                $this->addComment('�������� ����� �������� ������������ - '.$result['fullname']);
            }

            $sql = 'INSERT INTO
                       '.MonCms::$config['db_project_name'].'.`client_coordination_follower`
                    SET
                        `client_coordination_id` = '.$this->coordinationId.',
                        `user_id` = '.$this->followerUserId.',
                        `coordination_file_id` = '.$coordinationInfo['coordinationFileId'].'';

            MonCms::$db->query($sql);

        }
        else
        {
            $sql = 'INSERT INTO
                       '.MonCms::$config['db_project_name'].'.`client_coordination_follower`
                    SET
                        `client_coordination_id` = '.$this->coordinationId.',
                        `user_id` = '.$this->followerUserId.'';

            MonCms::$db->query($sql);
        }
    }


    /**
     * ����������� ���� ���������� ��� �������� ������������
     * @return array|null
     */
    public function getFollower()
    {
        $sql = 'SELECT
                    follower.client_coordination_id as clientCoordinationId,
                    follower.user_id as userId,
                    follower.coordination_file_id as coordinationFileId,
                    follower.coordination_file_status as coordinationFileStatus,
                    u.fullname,
                    u.phone,
                    u.email
                FROM
                    '.MonCms::$config['db_project_name'].'.client_coordination_follower as follower
                LEFT JOIN
                    '.MonCms::$config['db_devices_name'].'.users2 as u
                ON
                    follower.user_id = u.user_id
                WHERE
                    follower.client_coordination_id = '.$this->coordinationId;

        $result = MonCms::$db->fetchAllAssoc($sql);

        return $result;
    }


    /**
     * �������� ��������� ������������
     * @throws \system\exceptions\CDbMysqlException
     */
    public function deleteFollower()
    {
        $coordinationInfo = $this->getCoordinationInfo($this->coordinationId);

        if ($coordinationInfo['statusId'] != self::prototypeStatus)
        {
            $sql = 'SELECT
                        fullname
                    FROM
                        '.MonCms::$config['db_cms_name'].'.`users2`
                    WHERE
                        user_id = '.$this->followerUserId;

            $result = MonCms::$db->fetchRow($sql);

            $title = '������������ �������� � '.$this->contractNo;
            $msg = '��������� ������������ '.$result['fullname'].' ��� �������� � <a href="'.MonCms::$config['http_home'].'index.php?mod=coordinationContract&coordinationId='.$this->coordinationId.'">'.$coordinationInfo['contract_no'].'</a> ��� ������.';

            $this->sendNotification($title, $msg);
        }

        $sql = 'DELETE FROM
                    '.MonCms::$config['db_project_name'].'.client_coordination_follower
                WHERE
                    client_coordination_id = '.$this->coordinationId.'
                    AND user_id = '.$this->followerUserId.'';

        MonCms::$db->query($sql);
    }


    /**
     * ��� ���������� ������ ����� �������� ���� ����� ��� ���������� file_id � ���� ���������� ������������ � ��������� ������� ������������
     *
     * @param $fileId
     *
     * @throws \system\exceptions\CDbMysqlException
     */
    public function updateFollower($fileId)
    {
        $sql = 'UPDATE
                    '.MonCms::$config['db_project_name'].'.client_coordination_follower
                SET
                    coordination_file_id = '.$fileId.',
                    coordination_file_status = 0
                WHERE
                    client_coordination_id = '.$this->coordinationId.'';

        MonCms::$db->query($sql);
    }


    /**
     * ������� ���������� ���������� ������������
     * @return array|null
     */
    public function countFollower()
    {
        $sql = 'SELECT
                    user_id
                FROM
                    '.MonCms::$config['db_project_name'].'.`client_coordination_follower`
                WHERE
                    `client_coordination_id` = '.$this->coordinationId.'';

        $result = MonCms::$db->fetchRow($sql);

        return $result;
    }

    /**
     * ����������� ��� �������
     * @return array|null
     */
    public function getStatus()
    {
        $sql = 'SELECT
                    id,
                    coordination_status as status
                FROM
                    '.MonCms::$config['db_project_name'].'.client_coordination_status';

        $result = MonCms::$db->fetchAllAssoc($sql);

        return $result;
    }


    /**
     * ���������, ���������� �� �����
     * @return array|bool|null
     */
    public function checkBrand()
    {
        $sql = 'SELECT
                    brandId
                FROM
                    '.MonCms::$config['db_project_name'].'.client
                WHERE
                    clientId = '.$this->clientId.'';

        $result = MonCms::$db->fetchRow($sql);

        if ($result['brandId'] != null)
            $result['isBrand'] = true;
        else
            $result['isBrand'] = false;

        return $result;
    }

    /**
     * �������� � �������� �����
     * @throws \system\exceptions\CDbMysqlException
     */
    public function uploadFile()
    {
        $fileName = iconv_deep('utf-8', 'windows-1251', trim($_FILES['file']['name']));
        $uploadFileType = $_FILES['file']['type'];
        /**
         * ���� ���������� ����, �� ������ ���� ����, ���� ��� - �������.
         * �� ���������� upload ������ ���� ����� www-data
         */
        $dirPath = UPLOAD_DIR.'/clientCoordination/'.$this->coordinationId;
        if (!is_dir($dirPath))
            mkdir($dirPath, 0755, true);

        $allowFileType = ['application/msword', 'application/pdf', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'];

        if (isset($fileName) and $fileName != '')
        {
            if (in_array($uploadFileType, $allowFileType))
            {
                $fileName = time().'-'.$fileName;
                $filePath = $dirPath.'/'.totranslit($fileName);
                if (move_uploaded_file($_FILES['file']['tmp_name'], $filePath))
                {
                    $sql = 'INSERT INTO
                               '.MonCms::$config['db_project_name'].'.`client_coordination_files`
                            SET
                                `client_coordination_id` = "'.$this->coordinationId.'",
                                `user_id` = "'.$this->userId.'",
                                `add_date` = NOW(),
                                `file_name` = "'.totranslit($fileName).'",
                                `description` = "'.MonCms::$db->escapeString($this->uploadFileCoordinationDescription).'"';

                    MonCms::$db->query($sql);

                    $lastInsertId = MonCms::$db->lastInsertId();

                    $this->updateFollower($lastInsertId);

                }
            }
            else
                return false;

            $coordinationInfo = $this->getCoordinationInfo($this->coordinationId);

            if ($coordinationInfo['statusId'] != self::prototypeStatus)
            {
                $title = '������������ �������� � '.$coordinationInfo['contract_no'];
                $msg = '��������� ����� ������ ����� - '.trim(utf8ToWin($_FILES['file']['name'])).' ��� �������� � <a href="'.MonCms::$config['http_home'].'index.php?mod=coordinationContract&coordinationId='.$this->coordinationId.'">'.$coordinationInfo['contract_no'].'</a>.';

                $this->sendNotification($title, $msg);
            }

            return true;
        }
    }


    /**
     * ������������ ������������� ������ �����
     */
    public function coordinateFile()
    {

        $sql = 'UPDATE
                    '.MonCms::$config['db_project_name'].'.client_coordination_follower
                SET
                    coordination_file_status = 1
                WHERE
                    client_coordination_id = '.$this->coordinationId.'
                    AND coordination_file_id = '.$this->fileId.'
                    AND user_id = '.MonCms::$user->userId.'';

        MonCms::$db->query($sql);


        $coordinationInfo = $this->getCoordinationInfo($this->coordinationId);

        if ($coordinationInfo['statusId'] != self::prototypeStatus)
        {
            $sql = 'SELECT
                        fullname
                    FROM
                        '.MonCms::$config['db_cms_name'].'.`users2`
                    WHERE
                        user_id = '.MonCms::$user->userId;

            $result = MonCms::$db->fetchRow($sql);

            $title = '������������ �������� � '.$coordinationInfo['contract_no'];
            $msg = $result['fullname'].' ���������� ��������� ������ ��������� ��� �������� � <a href="'.MonCms::$config['http_home'].'index.php?mod=coordinationContract&coordinationId='.$this->coordinationId.'">'.$coordinationInfo['contract_no'].'</a>';

            $this->sendNotification($title, $msg);
        }

        // ������� ���������� ���������� ������������
        $sql = 'SELECT
                    count(client_coordination_id) as follower
                FROM
                    '.MonCms::$config['db_project_name'].'.`client_coordination_follower`
                WHERE
                    client_coordination_id = '.$this->coordinationId.'';

        $countFollower = MonCms::$db->fetchRow($sql)['follower'];

        // ������� ���������� �������������
        $sql = 'SELECT
                    count(client_coordination_id) as follower
                FROM
                    '.MonCms::$config['db_project_name'].'.`client_coordination_follower`
                WHERE
                    client_coordination_id = '.$this->coordinationId.'
                    AND coordination_file_status = 1';

        $agreeFollower = MonCms::$db->fetchRow($sql)['follower'];

        // ���� ����� ���������� ������������ � ����� ������������� �����, �� ��������� � ������ �����������
        if ($countFollower === $agreeFollower)
        {
            $sql = 'UPDATE
                        '.MonCms::$config['db_project_name'].'.client_coordination
                    SET
                        status_id = 3
                    WHERE
                        id = '.$this->coordinationId.'';

            MonCms::$db->query($sql);
        }
    }

    /**
     * ���������� �����������
     *
     * @param string $comment
     *
     * @return bool
     */
    public function addComment($comment = '')
    {
        $commentText = '';
        // ���� $comment ������, ������ ������ ������ �� �������. ���� �� ������, ������ ���� ��������� ������� ��� ���� ���������
        if ($comment == '')
        {
            if ($this->commentId != 0)
                $commentText .= '<a href="#comment_'.$this->commentId.'">����� �� ����������� �� '.$this->answerCommentUserName.'</a><br/>';
            $commentText .= $this->commentText;

            $coordinationInfo = $this->getCoordinationInfo($this->coordinationId);

            if ($coordinationInfo['statusId'] != self::prototypeStatus)
            {
                $title = '������������ �������� � '.$coordinationInfo['contract_no'];
                $msg = '�������� ����� ����������� � ������������ �������� � <a href="'.MonCms::$config['http_home'].'index.php?mod=coordinationContract&coordinationId='.$this->coordinationId.'">'.$coordinationInfo['contract_no'].'</a><br/>';
                $msg .= '����� �����������: '.$this->commentText;

                $this->sendNotification($title, $msg);
            }
        }
        else
        {
            $commentText = $comment;
        }

        if ($commentText != '')
        {
            $sql = 'INSERT INTO
                       '.MonCms::$config['db_project_name'].'.`client_coordination_comment`
                    SET
                        `client_coordination_id` = '.$this->coordinationId.',
                        `parent_id` = '.$this->parentId.',
                        `user_id` = '.MonCms::$user->userId.',
                        `add_date` = NOW(),
                        `comment` = "'.MonCms::$db->escapeString($commentText).'"';

            MonCms::$db->query($sql);

            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * ����������� ��� ����������� � ������������
     * @return array|null
     */
    public function getComment()
    {
        $sql = 'SELECT
                    comment.id,
                    comment.user_id as userId,
                    comment.add_date as addDate,
                    comment.comment as comment,
                    comment.parent_id as parentId,
                    u.fullname,
                    u.photo,
                    u.phone,
                    u.position,
                    u.department,
                    u.icq,
                    u.mobile
               FROM
                    '.MonCms::$config['db_project_name'].'.client_coordination_comment as comment
               LEFT JOIN
                    '.MonCms::$config['db_cms_name'].'.users2 as u
               ON
                    comment.user_id = u.user_id
               WHERE
                    comment.client_coordination_id = '.$this->coordinationId.'
               ORDER BY
                    comment.add_date DESC';

        $result = MonCms::$db->fetchAllAssoc($sql);

        return $result;
    }


    /**
     * ���������� ��������� �� �����
     *
     * @param string $title
     * @param string $msg
     */
    public function sendNotification($title = '', $msg = '')
    {

        $tmp = [];
        $follower = $this->getFollower();
        foreach ($follower as $value)
        {
            $tmp[] = $value['email'];
        }

        $email = implode(", ", $tmp);

        if ($email != '')
            send_mail($title, $msg, $email);
    }
}