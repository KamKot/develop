<?php
/**
 * Created by PhpStorm.
 * User: malyshevis
 * Date: 21.02.17
 * Time: 12:50
 */

namespace models;

use db_connect;
use system\AModel;
use system\CVarDump;
use system\MonCms;

class CIpAddressInspectModel extends AModel
{
    public function rules()
    {
        return [];
    }

    /**
     * ������� ���������� ip-������� ��������
     * @return array
     */
    public function countIp()
    {
        $sql = 'SELECT
                    `network_id`,
                    `mask`
                FROM
                    '.MonCms::$config['db_cms_name'].'.ip_address_inspect_network';

        $result = MonCms::$db->fetchAllAssoc($sql);

        $tmp = [];
        foreach ($result as $item)
            $tmp['networkIp'][$item['network_id']] = ((ip2long('255.255.255.255') - ip2long($item['mask'])) + 1);

        return $tmp;
    }


    /**
     * ����������� ����������� ������ � ������� � �������
     * @return array|null
     */
    public function getIp()
    {
        //����������� ���� ��������
        $sql = 'SELECT
                    ip_address_inspect_network.network_id,
                    ip_address_inspect_network.network,
                    ip_address_inspect_network.mask
                FROM
                    '.MonCms::$config['db_cms_name'].'.ip_address_inspect_network
                ORDER BY
                    ip_address_inspect_network.`network_id`';

        $networks = MonCms::$db->fetchAllAssoc($sql);

        //����������� ����������� ������
        $sql = 'SELECT
                    ip_address_inspect.`network_id`,
                    ip_address_inspect.`value`
                FROM
                    '.MonCms::$config['db_cms_name'].'.ip_address_inspect
                WHERE
                    ip_address_inspect.`date` = DATE_FORMAT(now(),"%Y-%m-%d")
                ORDER BY
                    ip_address_inspect.`network_id`';

        $result = MonCms::$db->fetchAllAssoc($sql);

        //��������� ����� ������
        $tmp = [];
        foreach ($networks as $item)
        {
            foreach ($result as $value)
            {
                if ($item['network_id'] == $value['network_id'])
                {
                    $tmp['ip'][$item['network_id']] = $value;
                    $tmp['ip'][$item['network_id']]['network_id'] = $item['network_id'];
                    $tmp['ip'][$item['network_id']]['network'] = $item['network'];
                    $tmp['ip'][$item['network_id']]['mask'] = count_chars(decbin(ip2long($item['mask'])), 1)[49];
                    break;
                }
                else
                {
                    $tmp['ip'][$item['network_id']]['network_id'] = $item['network_id'];
                    $tmp['ip'][$item['network_id']]['network'] = $item['network'];
                    $tmp['ip'][$item['network_id']]['mask'] = count_chars(decbin(ip2long($item['mask'])), 1)[49];
                    $tmp['ip'][$item['network_id']] += ['value' => '0'];
                }
            }
            $tmp['allIp'] += ((ip2long('255.255.255.255') - ip2long($item['mask'])) + 1);
        }


        foreach ($result as $value)
            $tmp['sumBusyIp'] += $value['value'];

        return $tmp;
    }

    /**
     * ����������� ���������� ������� ip �� �����
     * @return array
     */
    public function getDiagramIp()
    {
        $sql = 'SELECT
                    UNIX_TIMESTAMP(date)  AS `date`,
                    SUM(value) as countIp
                FROM
                    '.MonCms::$config['db_cms_name'].'.ip_address_inspect
                WHERE
                    `date` BETWEEN NOW() - INTERVAL 1 YEAR 
                    AND NOW() - INTERVAL - 1 DAY
                GROUP BY
                    `date`
                ORDER BY
                    `date`';

        $result = MonCms::$db->fetchAllAssoc($sql);

        $data = [];
        foreach ($result as $val)
            $data[] = '['.(($val['date'] + 3 * 3600) * 1000).', '.$val['countIp'].']';

        return $data;
    }

    /**
     * ����������� ����� ���������� ip �� �����
     * @return array
     */
    public function getDiagramTotalIp()
    {
        $sql = 'SELECT
                    UNIX_TIMESTAMP(date) AS `date`,
                    `value`
                FROM
                    '.MonCms::$config['db_cms_name'].'.ip_address_inspect_total
                WHERE
                    `date` BETWEEN NOW() - INTERVAL 1 YEAR 
                    AND NOW() - INTERVAL - 1 DAY
                ORDER BY
                    `date`';

        $result = MonCms::$db->fetchAllAssoc($sql);

        $data = [];
        foreach ($result as $val)
            $data[] = '['.(($val['date'] + 3 * 3600) * 1000).', '.$val['value'].']';

        return $data;
    }
}