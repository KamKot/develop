<?php
/**
 * Created by PhpStorm.
 * User: malyshevis
 * Date: 18.07.16
 * Time: 10:57
 */

namespace models;

use system\AModel;
use system\CVarDump;
use system\MonCms;


class CSupportModel extends AModel
{

    public $orderId;
    public $ticketId;
    public $houseId;
    public $planTime;
    public $dateFrom;
    public $dateTo;
    public $clientName;
    public $authorId;
    public $planDate;
    public $workStatusId;
    public $teamId;
    public $driverId;
    public $driverFilter;
    public $overtimeStatusId;
    public $description;
    public $newTeamName;
    public $planId = null;
    public $planFilter = null;
    public $page = 1;
    public $pageLen;
    public $start;
    public $limit;
    public $comment;
    public $contractNo;
    public $address;
    public $teamName;
    public $isCsv = null;
    public $isFilter = null;

    public function rules()
    {
        return ['supportPlanEdit' => [['subject'   => $this->planId,
                                       'validator' => 'id',
                                       'error'     => '�������� id ������',
                                       'errorId'   => 'planId'],

                                      ['subject'   => $this->orderId,
                                       'validator' => 'regexp',
                                       'pattern'   => '/[0-9]{5,6}/',
                                       'error'     => '�������� �������� ��������',
                                       'errorId'   => 'orderId'],

                                      ['subject'   => $this->ticketId,
                                       'validator' => 'regexp',
                                       'pattern'   => '/[0-9]{6}[\/][0-9]{2}/',
                                       'error'     => '�������� �������� ������',
                                       'errorId'   => 'ticketId'],

                                      ['subject'   => $this->houseId,
                                       'validator' => 'regexp',
                                       'pattern'   => '/[0-9]{5,6}',
                                       'error'     => '�������� �������� ������',
                                       'errorId'   => 'houseId'],

                                      ['subject'   => $this->planDate,
                                       'validator' => 'datetimeformat',
                                       'format'    => 'Y-m-d',
                                       'error'     => '�������� �������� ����',
                                       'errorId'   => 'planDate'],

                                      ['subject'   => $this->planTime,
                                       'validator' => 'datetimeformat',
                                       'format'    => 'H:i',
                                       'error'     => '�������� �������� �������',
                                       'errorId'   => 'planTime'],

                                      ['subject'   => $this->workStatusId,
                                       'validator' => 'range',
                                       'range'     => ['1', '2', '3', '4', '5'],
                                       'error'     => '�������� �������� ������� ������',
                                       'errorId'   => 'workStatusId'],

                                      ['subject'   => $this->teamId,
                                       'validator' => 'id',
                                       'error'     => '�������� �������� id ��������',
                                       'errorId'   => 'teamId'],

                                      ['subject'   => $this->driverId,
                                       'validator' => 'id',
                                       'allowEmpty' => true,
                                       'error'     => '�������� �������� id ��������',
                                       'errorId'   => 'driverId'],

                                      ['subject'   => $this->overtimeStatusId,
                                       'validator' => 'range',
                                       'range'     => ['1', '2'],
                                       'error'     => '�������� �������� id �����������',
                                       'errorId'   => 'overtimeStatusId'],
        ],
                'supportPlan'     => [['subject'   => $this->orderId,
                                       'validator' => 'regexp',
                                       'pattern'   => '/[0-9]{5,6}/',
                                       'error'     => '�������� �������� ��������',
                                       'errorId'   => 'orderId'],

                                      ['subject'   => $this->ticketId,
                                       'validator' => 'regexp',
                                       'pattern'   => '/[0-9]{6}[\/][0-9]{2}/',
                                       'error'     => '�������� �������� ������',
                                       'errorId'   => 'ticketId'],

                                      ['subject'   => $this->houseId,
                                       'validator' => 'regexp',
                                       'pattern'   => '/[0-9]{5,6}/',
                                       'error'     => '�������� �������� ������',
                                       'errorId'   => 'houseId'],

                                      ['subject'   => $this->planDate,
                                       'validator' => 'datetimeformat',
                                       'format'    => 'Y-m-d',
                                       'error'     => '�������� �������� ����',
                                       'errorId'   => 'planDate'],

                                      ['subject'   => $this->planTime,
                                       'validator' => 'datetimeformat',
                                       'format'    => 'H:i',
                                       'error'     => '�������� �������� �������',
                                       'errorId'   => 'planTime'],

                                      ['subject'   => $this->workStatusId,
                                       'validator' => 'range',
                                       'range'     => ['1', '2', '3', '4', '5'],
                                       'error'     => '�������� �������� ������� ������',
                                       'errorId'   => 'workStatusId'],

                                      ['subject'   => $this->teamId,
                                       'validator' => 'id',
                                       'error'     => '�������� �������� id ��������',
                                       'errorId'   => 'teamId'],

                                      ['subject'   => $this->driverId,
                                       'validator' => 'id',
                                       'allowEmpty'=> true,
                                       'error'     => '�������� �������� id ��������',
                                       'errorId'   => 'driverId'],

                                      ['subject'   => $this->overtimeStatusId,
                                       'validator' => 'range',
                                       'range'     => ['1', '2'],
                                       'error'     => '�������� �������� id �����������',
                                       'errorId'   => 'overtimeStatusId'],

                ],
                'filter'          => [['subject'    => $this->orderId,
                                       'validator'  => 'regexp',
                                       'pattern'    => '/[0-9]{5,6}/',
                                       'allowEmpty' => true,
                                       'error'      => '�������� �������� ��������',
                                       'errorId'    => 'orderId'],

                                      ['subject'    => $this->ticketId,
                                       'validator'  => 'regexp',
                                       'pattern'    => '/[0-9]{6}[\/][0-9]{2}/',
                                       'allowEmpty' => true,
                                       'error'      => '�������� �������� ������',
                                       'errorId'    => 'ticketId'],

                                      ['subject'    => $this->houseId,
                                       'validator'  => 'regexp',
                                       'pattern'    => '/[0-9]{5,6}/',
                                       'allowEmpty' => true,
                                       'error'      => '�������� �������� ������',
                                       'errorId'    => 'houseId'],

                                      ['subject'    => $this->dateFrom,
                                       'validator'  => 'datetimeformat',
                                       'format'     => 'Y-m-d',
                                       'allowEmpty' => true,
                                       'error'      => '�������� �������� ����',
                                       'errorId'    => 'dateFrom'],

                                      ['subject'    => $this->dateTo,
                                       'validator'  => 'datetimeformat',
                                       'format'     => 'Y-m-d',
                                       'allowEmpty' => true,
                                       'error'      => '�������� �������� �������',
                                       'errorId'    => 'dateTo'],

                                      ['subject'    => $this->clientName,
                                       'validator'  => 'regexp',
                                       'pattern'    => '/(^[�-��-ߨ�]{1,100}$)|(^[�-��-ߨ�]+[0-9]{1,100}$)|(^[0-9]+[�-��-ߨ�]{1,100}$)|([0-9]{1,100}$/',
                                       'allowEmpty' => true,
                                       'error'      => '��������� ����� ������',
                                       'errorId'    => 'clientName'],

                                      ['subject'    => $this->authorId,
                                       'validator'  => 'id',
                                       'allowEmpty' => true,
                                       'error'      => '�������� �������� id ����������1',
                                       'errorId'    => 'authorId'],

                                      ['subject'    => $this->workStatusId,
                                       'validator'  => 'range',
                                       'range'      => ['1', '2', '3', '4', '5'],
                                       'allowEmpty' => true,
                                       'error'      => '�������� �������� ������� ������',
                                       'errorId'    => 'workStatusId'],

                                      ['subject'    => $this->teamId,
                                       'validator'  => 'id',
                                       'allowEmpty' => true,
                                       'error'      => '�������� �������� id ��������',
                                       'errorId'    => 'teamId'],

                                      ['subject'    => $this->driverId,
                                       'validator'  => 'id',
                                       'allowEmpty' => true,
                                       'error'      => '�������� �������� id ��������',
                                       'errorId'    => 'driverId'],

                                       ['subject'    => $this->driverFilter,
                                       'validator'  => 'range',
                                       'range'      => ['0','1'],
                                       'error'      => '�������� �������� id ��������',
                                       'errorId'    => 'driverFilter'],

                                      ['subject'    => $this->overtimeStatusId,
                                       'validator'  => 'range',
                                       'range'      => ['1', '2'],
                                       'allowEmpty' => true,
                                       'error'      => '�������� �������� id �����������',
                                       'errorId'    => 'overtimeStatusId'],

                ],
                'planId'          => [['subject'   => $this->planId,
                                       'validator' => 'id',
                                       'error'     => '�������� id ������',
                                       'errorId'   => 'planId'],
                ],
                'page'            => [['subject'   => $this->page,
                                       'validator' => 'id',
                                       'error'     => '�������� id ��������',
                                       'errorId'   => 'page'],
                ],
        ];
    }


    /**
     * ��������� ���� ��������������� �������, ���� ������ �� id
     *
     * @return array|null
     */
    public function getAllPlan()
    {
        $where = '';
        $limit = 'LIMIT '.$this->start.','.$this->pageLen.'';
        if (isset($this->planId))
        {
            $where = 'WHERE s.plan_id = '.$this->planId;
            $limit = '';
        }

        if(!$this->isCsv == null)
            $limit = '';

        // ����������
        if ($this->isFilter == 1)
        {
            if (!$this->ticketId == null or !$this->ticketId == '')
                $where == '' ? $where = 'WHERE s.ticket_id LIKE "'.$this->ticketId.'%" ' : $where .= ' and s.ticket_id LIKE "'.$this->ticketId.'%" ';

            if (!$this->orderId == null or !$this->orderId == '')
                $where == '' ? $where = 'WHERE s.order_id LIKE "'.$this->orderId.'%" ' : $where .= ' and s.order_id LIKE "'.$this->orderId.'%" ';

            if (!$this->houseId == null or !$this->houseId == '')
                $where == '' ? $where = 'WHERE s.house_id LIKE "'.$this->houseId.'%" ' : $where .= ' and s.house_id LIKE "'.$this->houseId.'%" ';

            if ((!$this->dateFrom == null or !$this->dateFrom == '') and (!$this->dateTo == null or !$this->dateTo == ''))
                $where == '' ? $where = 'WHERE s.plan_datetime > "'.$this->dateFrom.'" and s.plan_datetime <"'.$this->dateTo.'" ' : $where .= ' and s.plan_datetime > "'.$this->dateFrom.'" and s.plan_datetime <"'.$this->dateTo.'" ';

            if (!$this->clientName == null or !$this->clientName == '')
                $where == '' ? $where = 'WHERE c.fullname LIKE "%'.$this->clientName.'%" ' : $where .= ' and c.fullname LIKE "%'.$this->clientName.'%" ';

            if (!$this->authorId == null or !$this->authorId == '')
                $where == '' ? $where = 'WHERE s.author_id  = "'.$this->authorId.'" ' : $where .= ' and s.author_id  = "'.$this->authorId.'" ';

            if (!$this->workStatusId == null or !$this->workStatusId == '')
                $where == '' ? $where = 'WHERE s.status_id = "'.$this->workStatusId.'" ' : $where .= ' and s.status_id = "'.$this->workStatusId.'" ';

            if (!$this->teamId == null or !$this->teamId == '')
                $where == '' ? $where = 'WHERE s.team_id = "'.$this->teamId.'" ' : $where .= ' and s.team_id = "'.$this->teamId.'" ';

            if (!$this->driverId == null and !$this->driverId == '')
                $where == '' ? $where = 'WHERE s.driver_id = "'.$this->driverId.'" ' : $where .= ' and s.driver_id ="'.$this->driverId.'" ';

            if ($this->driverFilter == 0 and $this->driverId == '')
                $where == '' ? $where = 'WHERE s.driver_id is null' : $where .= ' and s.driver_id is null ';

            if (!$this->overtimeStatusId == null or !$this->overtimeStatusId == '')
                $where == '' ? $where = 'WHERE s.overtime_id = "'.$this->overtimeStatusId.'" ' : $where .= ' and s.overtime_id = "'.$this->overtimeStatusId.'" ';
        }

        $sql = 'SELECT
                    `s`.`plan_id` as `planId`,
                    `s`.`ticket_id` as `ticketId`,
                    `s`.`order_id` as `orderId`,
                    `s`.`house_id` as `houseId`,
                    `s`.`author_id` as `creator`,
                    `s`.`plan_datetime` as `planDatetime`,
                    `s`.`team_id` as `teamId`,
                    `s`.`driver_id` as `driverId`,
                    `s`.`status_id` as `statusId`,
                    `s`.`overtime_id` as `overtimeDeviceId`,
                    `s`.`description` as `description`,
                    `c`.`fullname` as `clientName`,
                    `c`.`contract_no` as `contractNo`,
                    `fa`.`city_name` as `city`,
                    `fa`.`street_name` as `street`,
                    `fa`.`house_name` as `house`,
                    `spc`.`description` as `overtimeDescription`,
                    `spt`.`team_name` as `teamName`,
                    `sps`.`description` as `statusDescription`,
                    `us`.`fullname` as `fullname`,
                    `driver`.`fullname` as `driverName`
                FROM
                    '.MonCms::$config['db_cms_name'].'.`support_plan` as `s`
                LEFT JOIN
                    '.MonCms::$config['db_fst_name'].'.`clients` as `c`
                ON
                    `s`.`order_id` = `c`.`order_id`
                LEFT JOIN
                    '.MonCms::$config['db_fst_name'].'.`fst_address` as `fa`
                ON
                    `s`.`house_id` = `fa`.`house_id`
                LEFT JOIN
                    '.MonCms::$config['db_cms_name'].'.`support_plan_overtime` as `spc`
                ON
                    `s`.`overtime_id` = `spc`.`overtime_id`
                LEFT JOIN
                    '.MonCms::$config['db_cms_name'].'.`support_plan_team` as `spt`
                ON
                    `s`.`team_id` = `spt`.`team_id`
                LEFT JOIN
                    '.MonCms::$config['db_cms_name'].'.`support_plan_status` as `sps`
                ON
                    `s`.`status_id` = `sps`.`status_id`
                LEFT JOIN
                    '.MonCms::$config['db_cms_name'].'.`users2` as `driver`
                ON
                    `s`.`driver_id` = `driver`.`user_id`
                LEFT JOIN
                    '.MonCms::$config['db_cms_name'].'.`users2` as `us`
                ON
                    `s`.`author_id` = `us`.`user_id`
                '.$where.'
                ORDER BY
                    `planDatetime` DESC
                '.$limit.'';

        if (isset($this->planId))
            $result = $this->db->fetchRow($sql);
        else
            $result = $this->db->fetchAllAssoc($sql);


        return $result;
    }

    /**
     * ������� ���������� �������
     * @return mixed
     */
    public function countAllPlan()
    {
        $sql = 'SELECT
                    COUNT(plan_id) as `count`
                FROM
                    '.MonCms::$config['db_cms_name'].'.`support_plan`';

        $result = $this->db->fetchRow($sql);

        return $result['count'];
    }

    /**
     * ��������� ������������ �� ��������� ������
     * @return array|null
     */
    public function getPlanComment()
    {
        $sql = 'SELECT
                    `comm`.`comment`,
                    `comm`.`add_date` as `addDate`,
                    `us`.`fullname` as `fullname`,
                    `us`.`department` as `department`,
                    `us`.`position` as `position`,
                    `us`.`photo` as `photo`,
                    `us`.`icq` as `icq`,
                    `us`.`phone` as `phone`,
                    `us`.`mobile` as `mobile`
                FROM
                    '.MonCms::$config['db_cms_name'].'.`support_plan_comment` as `comm`
                LEFT JOIN
                    '.MonCms::$config['db_cms_name'].'.`users2` as `us`
                ON
                    `comm`.`user_id` = `us`.`user_id`
                WHERE
                    `support_plan_id` = '.$this->planId.'
                ORDER BY
                    `comm`.`add_date` DESC';

        $result = $this->db->fetchAllAssoc($sql);

        return $result;
    }

    /**
     * ��������� ������ � ����
     * @throws \system\exceptions\CDbMysqlException
     */
    public function addPlan()
    {
        #CVarDump::dump($this->driverId);
        #exit;
        $dateTime = $this->planDate.' '.$this->planTime;
        $driverId = '';
        $this->driverId == '' ? $driverId = 'NULL' : $driverId = $this->driverId;
        $sql = 'INSERT INTO
                    '.MonCms::$config['db_cms_name'].'.`support_plan`
                VALUES
                    ("","'.$this->ticketId.'",'.$this->orderId.','.$this->houseId.', '.MonCms::$user->userId.',
                    "'.$dateTime.'", '.$this->teamId.','.$driverId.', '.$this->workStatusId.',
                    '.$this->overtimeStatusId.',"'.$this->db->escapeString($this->description).'")';

        $this->db->query($sql);

    }

    /**
     * �������������� ������
     * @throws \system\exceptions\CDbMysqlException
     */
    public function editPlan()
    {
        $currentPLan = $this->getAllPlan();
        $driverId = '';
        $set = '';
        $this->driverId == '' ? $driverId = 'NULL' : $driverId = $this->driverId;
        if ($currentPLan['ticketId'] != $this->ticketId)
        {
            #$set == '' ? $set = 'SET `ticket_id` = "'.$this->ticketId.'"' : $set = ',`ticket_id` = "'.$this->ticketId.'"';
            $this->comment .= '����� ������ �� ���������: '.$currentPLan['ticketId'].'. '.'������� ��: '.$this->ticketId.'<br>';
        }

        if ($currentPLan['orderId'] != $this->orderId)
        {
            #$set == '' ? $set = 'SET `order_id` = "'.$this->orderId.'"' : $set = ',`order_id` = "'.$this->orderId.'"';
            $this->comment .= '����� �������� �� ���������: '.$currentPLan['contractNo'].'. '.'������� ��: '.$this->db->escapeString($this->contractNo).'<br>';
        }

        if ($currentPLan['houseId'] != $this->houseId)
        {
            #$set == '' ? $set = 'SET `house_id` = "'.$this->houseId.'"' : $set = ',`house_id` = "'.$this->houseId.'"';
            $this->comment .= '����� �� ���������: '.$currentPLan['city'].', '.$currentPLan['street'].', '.$currentPLan['house'].'. '.'������� ��: '.$this->db->escapeString($this->address).'<br>';
        }

        if ($currentPLan['planDatetime'] != ($this->planDate.' '.$this->planTime.':00'))
        {
            #$set == '' ? $set = 'SET `plan_datetime` = "'.$this->planDate.' '.$this->planTime.'"' : $set = ',`plan_datetime` = "'.$this->planDate.' '.$this->planTime.'"';
            $this->comment .= '���� � ����� �� ���������: '.$currentPLan['planDatetime'].'. '.'�������� ��: '.$this->planDate.' '.$this->planTime.'<br>';
        }

        if ($currentPLan['teamId'] != $this->teamId)
        {
            #$set == '' ? $set = 'SET `team_id` = "'.$this->teamId.'"' : $set = ',`team_id` = "'.$this->teamId.'"';
            $this->comment .= '������������� ������� �� ���������: '.$currentPLan['teamName'].'<br>';
        }

        if ($currentPLan['driverId'] != $this->driverId)
        {
            #$set == '' ? $set = 'SET `driver_id` = "'.$this->driverId.'"' : $set = ',`driver_id` = "'.$this->driverId.'"';
            $currentPLan['driverName'] == '' ? $currentPLan['driverName'] = "��� ��������" : $currentPLan['driverName'];
            $this->comment .= '������������� �������� �� ���������: '.$currentPLan['driverName'].'<br>';
        }

        if ($currentPLan['statusId'] != $this->workStatusId)
        {
           # $set == '' ? $set = 'SET `status_id` = "'.$this->workStatusId.'"' : $set = ',`status_id` = "'.$this->workStatusId.'"';
            $this->comment .= '������ �� ���������: '.$currentPLan['statusDescription'].'<br>';
        }

        if ($currentPLan['overtimeDeviceId'] != $this->overtimeStatusId)
        {
           # $set == '' ? $set = 'SET `overtime_id` = "'.$this->overtimeStatusId.'"' : $set = ',`overtime_id` = "'.$this->overtimeStatusId.'"';
            $this->comment .= '������ ����������� �� ���������: '.$currentPLan['overtimeDescription'].'<br>';
        }

        if (!$this->comment == '')
            $this->addComment();

        $sql = 'UPDATE
                    '.MonCms::$config['db_cms_name'].'.`support_plan`
                SET
                    `ticket_id` = "'.$this->ticketId.'",
                    `order_id` = '.$this->orderId.',
                    `house_id` = '.$this->houseId.',
                    `plan_datetime` = "'.$this->planDate.' '.$this->planTime.'",
                    `team_id` = '.$this->teamId.',
                    `driver_id` = '.$driverId.',
                    `status_id` = '.$this->workStatusId.',
                    `overtime_id` = '.$this->overtimeStatusId.',
                    `description` = "'.$this->db->escapeString($this->description).'"
                WHERE
                    `plan_id` = '.$this->planId.'';

        $this->db->query($sql);
    }

    /**
     * ��������� ����������� � ������� � ��������
     * @return array
     */
    public function getTeam()
    {
        $sql = 'SELECT
                    `team_id` as `teamId`,
                    `team_name` as `team`
                FROM
                    '.MonCms::$config['db_cms_name'].'.`support_plan_team`
                WHERE
                    `banned` = 0';

        $result = $this->db->fetchAllAssoc($sql);

        return $result;
    }

    /**
     * ��������� ����������� � ������� � ���������
     * @return array
     */
    public function getDriver()
    {
        $sql = 'SELECT
					user_id AS `userId`,
					fullname AS driverName
				FROM
					'.MonCms::$config['db_cms_name'].'.users2
				WHERE
					departmentId in (10,15,19,24)
					AND `position` = "��������"
					AND `user_banned` = 0
				ORDER BY
						driverName';

        $result = $this->db->fetchAllAssoc($sql);

        $tmp[0]['userId'] = null;
        $tmp[0]['driverName'] = '��� ��������';
        $i = 1;
        foreach ($result as $val)
        {
            $tmp[$i]['userId'] = $val['userId'];
            $tmp[$i]['driverName'] = $val['driverName'];
            $i++;
        }

        return $tmp;
    }

    /**
     * ��������� ����������� � ������� ������������
     * @return array
     */
    public function getOvertimeStatus()
    {
        $sql = 'SELECT
                    `overtime_id` as overtimeStatusId,
                    `description`
                FROM
                    '.MonCms::$config['db_cms_name'].'.`support_plan_overtime`
                ORDER BY `overtime_id` DESC';

        $result = $this->db->fetchAllAssoc($sql);

        return $result;
    }

    /**
     * ��������� ����������� � ������� � �������� ����������
     * @return array
     */
    public function getWorkStatus()
    {
        $sql = 'SELECT
                    `status_id` as `statusId`,
                    `description`
                FROM
                    '.MonCms::$config['db_cms_name'].'.`support_plan_status`';

        $result = $this->db->fetchAllAssoc($sql);

        return $result;
    }

    /**
     * ���������� ����� �������
     * @throws \system\exceptions\CDbMysqlException
     */
    public function addNewTeam()
    {
        $sql = 'INSERT INTO
                    '.MonCms::$config['db_cms_name'].'.`support_plan_team`
                 VALUES
                    ("","'.$this->db->escapeString($this->newTeamName).'", 0)';

        $this->db->query($sql);
    }

    /**
     * ��������� ������� � ���
     * @throws \system\exceptions\CDbMysqlException
     */
    public function addTeamInBan()
    {
        $sql = 'UPDATE
                    '.MonCms::$config['db_cms_name'].'.`support_plan_team`
                SET
                    `banned` = 1
                WHERE
                    `team_id` = "'.$this->teamId.'"';

        $this->db->query($sql);
    }

    public function addComment()
    {
        $sql = 'INSERT INTO
                    '.MonCms::$config['db_cms_name'].'.`support_plan_comment`
               VALUES
                    ("",'.$this->planId.','.MonCms::$user->userId.',"'.$this->db->escapeString($this->comment).'",NOW())';

        $result = $this->db->query($sql);


    }

}