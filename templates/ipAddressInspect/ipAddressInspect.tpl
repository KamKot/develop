<div class="row-fluid">
    <div class="span7">
        <div id="placeholder" style="width:700px;height:480px"></div>
        <div class="" id="legend" style="float:left;"></div>
    </div>
    <div class="span5">
        <table class="table table-bordered">
            <tr>
                <th>����</th>
                <th>���������� ������� IP</th>
                <th>���������� ��������� IP</th>
            </tr>
            {% for i in ip %}
                <tr>
                    <td>{{ i.network }} / {{ i.mask }}</td>
                    <td>{{ i.value }}</td>
                    <td>
                        {% for index,z in networkIp %}
                            {% if index == i.network_id %}
                                {{ z - i.value }}
                            {% endif %}
                        {% endfor %}
                    </td>
                </tr>
            {% endfor %}
            <tr>
                <td><b>�����: </b>{{ allIp }}</td>
                <td>{{ sumBusyIp }}</td>
                <td>{{ sumFreeIp }}</td>
            </tr>
        </table>
    </div>
</div>
<script id="source" language="javascript" type="text/javascript">

    // ������ ��� ��������
    var data = [
        {label: "������� ���������� ������� IP", data: [{{ diagramIp }}]},
        {label: "�����", data: [{{ totalIp }}]}
    ];
    // �������� �������
    var options = {
        series: {
            lines: {
                show: true,
                lineWidth: 2
            },
            points: {show: true}
        },
        legend: {
            container: $("#legend")
        },
        xaxis: {
            mode: "time",
            timeformat: "%y-%0m-%0d",
            color: "red",
            minTickSize: [1, "day"]
        },
        grid:
        {
            hoverable: true,
        }
    };
    // ������� ������
    $.plot('#placeholder', data, options);

    $('<div id="tooltip"></div>').css(
        {
            position: 'absolute',
            display: 'none',
            border: '1px solid #fdd',
            padding: "2px",
            "background-color": '#fee',
            opacity: 0.80
        }).appendTo('body');

    $('#placeholder').bind('plothover', function (event, pos, item)
    {
        if (item)
        {
            var x = item.datapoint[0],
                y = item.datapoint[1];
            $('#tooltip').html(y)
                .css({top: item.pageY + 5, left: item.pageX + 5})
                .fadeIn(200);
        }
        else
        {
            $('#tooltip').hide();
        }
    });
</script>