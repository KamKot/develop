<style>
    th.column-order-th
    {
        position: relative;
        height: 130px;
    }
</style>
<script type="text/javascript">
    $(document).ready(function ()
    {
        $(function()
        {
            var position = $(".slidemenu").offset();
            $(window).scroll(function()
            {
                if ($(window).scrollLeft() <= position.left)
                    $(".slidemenu").stop().animate({marginLeft: 0});
                else
                    $(".slidemenu").stop().animate({marginLeft: $(window).scrollLeft()});
            });
        });

        $('#filterAddress').keydown(function (e) {
             if(e.keyCode == 8 || e.keyCode == 46)
             {
                 $('#filterHouseId').val('');
             }
         });

        $('#filterAddress').autocomplete(
        {
            serviceUrl: './modules/ajax/autocomplete.php', // �������� ��� ��������� �������� ��������������
            minChars: 2, // ����������� ����� ������� ��� ������������ ��������������
            maxHeight: 400, // ������������ ������ ������ ���������, � ��������
            width: 500, // ������ ������
            maxWidth: 800, // ������ ������
            zIndex: 9999, // z-index ������
            deferRequestBy: 200, // �������� ������� (����), �� ������, ���� �� �� ����� ����� ������� ��������, ���� ������������ ��������. � ������ ������ 300.
            params: {type: 'fstAddressList'}, // �������������� ���������
            onSelect: function (data) {
                $('#filterAddress').val(data.value);
                $('#filterHouseId').val(data.data);
            } // Callback �������, ������������� �� ����� ������ �� ������������ ���������,
        });

        $('#manager').autocomplete(
        {
            serviceUrl: './modules/ajax/autocomplete.php', // �������� ��� ��������� �������� ��������������
            minChars: 2, // ����������� ����� ������� ��� ������������ ��������������
            maxHeight: 400, // ������������ ������ ������ ���������, � ��������
            width: 500, // ������ ������
            maxWidth: 800, // ������ ������
            zIndex: 9999, // z-index ������
            deferRequestBy: 200, // �������� ������� (����), �� ������, ���� �� �� ����� ����� ������� ��������, ���� ������������ ��������. � ������ ������ 300.
            params: {type: 'users_list'}, // �������������� ���������
            onSelect: function (data) {
                $('#manager').val(data.value);
            } // Callback �������, ������������� �� ����� ������ �� ������������ ���������,
        });

        $('#projectManager').autocomplete(
        {
            serviceUrl: './modules/ajax/autocomplete.php', // �������� ��� ��������� �������� ��������������
            minChars: 2, // ����������� ����� ������� ��� ������������ ��������������
            maxHeight: 400, // ������������ ������ ������ ���������, � ��������
            width: 500, // ������ ������
            maxWidth: 800, // ������ ������
            zIndex: 9999, // z-index ������
            deferRequestBy: 200, // �������� ������� (����), �� ������, ���� �� �� ����� ����� ������� ��������, ���� ������������ ��������. � ������ ������ 300.
            params: {type: 'users_list'}, // �������������� ���������
            onSelect: function (data) {
                $('#projectManager').val(data.value);
            } // Callback �������, ������������� �� ����� ������ �� ������������ ���������,
        });

        $('#seller').keydown(function (e) {
             if(e.keyCode == 8 || e.keyCode == 46)
             {
                 $('#filterSellerId').val('');
             }
         });

        $('#seller').autocomplete(
        {
            serviceUrl: './modules/ajax/autocomplete.php', // �������� ��� ��������� �������� ��������������
            minChars: 2, // ����������� ����� ������� ��� ������������ ��������������
            maxHeight: 400, // ������������ ������ ������ ���������, � ��������
            width: 500, // ������ ������
            maxWidth: 800, // ������ ������
            zIndex: 9999, // z-index ������
            deferRequestBy: 200, // �������� ������� (����), �� ������, ���� �� �� ����� ����� ������� ��������, ���� ������������ ��������. � ������ ������ 300.
            params: {type: 'ur_seller'},
            // �������������� ���������
            onSelect: function (data) {
                $('#filterSellerId').val(data.data);
            }// Callback �������, ������������� �� ����� ������ �� ������������ ���������,
        });

});
</script>

<!--<div class="navigation" style="text-align:center;">{{ pages | raw }}</div>-->

<br />
<form action="./index.php?mod=planCapitalCost" method="get">
    <input type="hidden" name="mod" value="planCapitalCost" />
    <div class="row-fluid slidemenu">
        <div class="span12 row">
            <div data-date-format="yyyy-mm-dd" data-date="" class="input-append date dp">
                <input type="text" placeholder="���� �" name="filter[dateFrom]" readonly="readonly" size="10" class="span7" value="{{ filter.dateFrom }}">
                <span class="add-on"><i class="icon-calendar"></i></span>
                <span class="add-on"><i class="icon-trash"></i></span>
            </div>
            <div data-date-format="yyyy-mm-dd" data-date="" class="input-append date dp">
                <input type="text" placeholder="���� ��" name="filter[dateTo]" readonly="readonly" size="10" class="span7" value="{{ filter.dateTo }}">
                <span class="add-on"><i class="icon-calendar"></i></span>
                <span class="add-on"><i class="icon-trash"></i></span>
            </div>
            <div style="display: inline-block; margin-bottom: 10px; vertical-align: middle;">
                <input type="submit" value="������" class="btn btn-info" />
                <a href="./index.php?mod=planCapitalCost" class="btn btn-info">�����</a>
                <a href="{{ csv }}" class="btn bhelp" name="csv" data-original-title="������� � CSV � ������ �������" data-placement="right"><i class="ico_sprite ico_excel"></i></a>
               <!-- <input type=checkbox name=urWithoutSmr value="1">������� �� ��� ���-->
            </div>
            <div class="radio inline">
                {% if filter.withOutFilter == 1 %}
                    <input id="withOutFilter" type="radio" name="filter[radio]" value="withOutFilter" style="margin: 2px 0 0 !important;" checked>��� �������
                {% else %}
                    <input id="withOutFilter" type="radio" name="filter[radio]" value="withOutFilter" style="margin: 2px 0 0 !important;">��� �������
                {% endif %}
            </div>
            <div class="radio inline">
                {% if filter.urWithoutSmr == 1 %}
                    <input id="urWithoutSmr" type="radio" name="filter[radio]" value="urWithoutSmr" style="margin: 2px 0 0 !important;" checked>������� �� ��� ���
                {% else %}
                    <input id="urWithoutSmr" type="radio" name="filter[radio]" value="urWithoutSmr" style="margin: 2px 0 0 !important;">������� �� ��� ���
                {% endif %}
            </div>
            <div class="radio inline">
                {% if filter.bcRtk == 1 %}
                    <input class="" id="bcRtk" type="radio" name="filter[radio]" value="bcRtk" style="margin: 2px 0 0 !important;" checked>�� ���
                {% else %}
                    <input class="" id="bcRtk" type="radio" name="filter[radio]" value="bcRtk" style="margin: 2px 0 0 !important;">�� ���
                {% endif %}
            </div>
            <div class="radio inline">
                {% if filter.networkModernization == 1 %}
                    <input class="" id="bcRtk" type="radio" name="filter[radio]" value="networkModernization" style="margin: 2px 0 0 !important;" checked>������������ ����
                {% else %}
                    <input class="" id="bcRtk" type="radio" name="filter[radio]" value="networkModernization" style="margin: 2px 0 0 !important;">������������ ����
                {% endif %}
            </div>
            <!--<div class="myswitch divMarginBottom small span12" style="display: inline-block;">
                <div class=span6 style="display: inline-block;">
                    {% if filter.urWithoutSmr == 1 %}
                        <input class="myswitch-input" id="urWithoutSmr" type="checkbox" name="filter[urWithoutSmr]" checked>
                    {% else %}
                        <input class="myswitch-input" id="urWithoutSmr" type="checkbox" name="filter[urWithoutSmr]">
                    {% endif %}
                    <label class="myswitch-paddle" for="urWithoutSmr1">
                        <span class="show-for-sr">������� �� ��� ���(�������� ��� �� �����������)</span>
                    </label>

                </div>

                <div class=span4 style="display: inline-block;">
                    {% if filter.bcRtk == 1 %}
                        <input class="myswitch-input" id="bcRtk" type="checkbox" name="filter[bcRtk]" checked>
                    {% else %}
                        <input class="myswitch-input" id="bcRtk" type="checkbox" name="filter[bcRtk]">
                    {% endif %}
                    <label class="myswitch-paddle" for="bcRtk">
                        <span class="show-for-sr">�� ���</span>
                    </label>
                </div>
            </div>-->
        </div>
    </div>


    <div class="row" style="padding-left: 1.3%;">
        <table class="table table-bordered">
            <tr>
                <th>
                    <!--<input type="text" id="contract" name="filter[teoId]" placeholder="��� Id" class="span12" value="{{ filter.teoId }}" />-->
                    ��� Id
                </th>
                <th class="column-order-th">
                    <div class="column-order-title">
                        <input type="text"  name="filter[teoProjectName]" placeholder="�������� ���" class="span2" value="{{ filter.teoProjectName }}" />
                    </div>
                    <!--<div class="column-order-button">
                        <div class="checkbox" style="padding-left: 0;">
                            {% if isTeoId == 1 %}
                                <input name="filter[isTeoId]" type="checkbox" checked style="margin-left: 0 !important;">��� ���
                            {% else %}
                                <input name="filter[isTeoId]" type="checkbox" style="margin-left: 0 !important;">��� ���
                            {% endif %}
                        </div>
                    </div>-->
                </th>
                <th class="column-order-th">
                    <input type="text" id="filterAddress" name="filterAddress" class="span3" placeholder="�����" value="{{ filterAddress }}" />
                    <input type="hidden" id="filterHouseId" name="filter[houseId]"  value="" />
                </th>
                <th class="column-order-th">
                    �������
                </th>
                <th class="column-order-th">
                    �����
                </th>
                <th class="column-order-th span10">
                    <!--<div class="column-order-title">
                        <input type="text" id="manager"  name="filter[manager]" placeholder="��������" class="span12" value="{{ filter.manager }}" />
                    </div>
                    <div class="column-order-button">
                        <div class="checkbox" style="padding-left: 0;">
                            {% if isManager == 1 %}
                                <input name="filter[isManager]" type="checkbox" checked style="margin-left: 0 !important;">�������� �� ������
                            {% else %}
                                <input name="filter[isManager]" type="checkbox" style="margin-left: 0 !important;">�������� �� ������
                            {% endif %}
                        </div>
                    </div>-->
                    ��������
                </th>
                <th>
                    ��� ������
                </th>
                <th>
                    �����
                </th>
                <!--<th class="column-order-th">
                    <div class="column-order-title">
                        <input type="text" id="projectManager"  name="filter[projectManager]" placeholder="��� �������" class="span12" value="{{ filter.projectManager }}" />
                    </div>
                    <div class="column-order-button">
                        <div class="checkbox" style="padding-left: 0;">
                        {% if isProjectManager == 1 %}
                            <input name="filter[isProjectManager]" type="checkbox" checked style="margin-left: 0 !important;">�������� �� ������
                        {% else %}
                            <input name="filter[isProjectManager]" type="checkbox" style="margin-left: 0 !important;">�������� �� ������
                        {% endif %}
                        </div>
                    </div>
                </th>-->
                <!--<th class="column-order-th">
                    <div class="column-order-title">
                        ��. ���� ����������� �����
                        <div data-date-format="yyyy-mm-dd" data-date="" class="input-append date dp">
                            <input type="text" class="span6" style="font-size: 10px;" placeholder="����" name="filter[planDateInstallService]" readonly="readonly" size="10" value="{{ filter.planDateInstallService }}">
                            <span class="add-on"><i class="icon-calendar"></i></span>
                            <span class="add-on"><i class="icon-trash"></i></span>
                        </div>
                    </div>
                    <div class="column-order-button">
                        <div class="checkbox" style="padding-left: 0;">
                            {% if isPlanDateInstallService == 1 %}
                                <input name="filter[isPlanDateInstallService]" type="checkbox" checked style="margin-left: 0 !important;">���� �� �������
                            {% else %}
                                <input name="filter[isPlanDateInstallService]" type="checkbox" style="margin-left: 0 !important;">���� �� �������
                            {% endif %}
                        </div>
                    </div>
                </th>-->
                <th class="column-order-th">
                    <div class="column-order-title">
                        ��. ���� ��������� �������
                    </div>
                    <!--
                    <div class="column-order-button">
                        <div data-date-format="yyyy-mm-dd" data-date="" class="input-append date dp">
                            <input type="text" class="span6" style="font-size: 10px;" placeholder="����" name="filter[planDateFinishBuild]" readonly="readonly" size="10" value="{{ filter.planDateFinishBuild }}">
                            <span class="add-on"><i class="icon-calendar"></i></span>
                            <span class="add-on"><i class="icon-trash"></i></span>
                        </div>
                    </div>
                    -->
                    <!--<div class="column-order-button">
                        <div class="checkbox" style="padding-left: 0;">
                            {% if isFinishDate == 1 %}
                                <input name="filter[isFinishDate]" type="checkbox" checked style="margin-left: 0 !important;">���� �� �������
                            {% else %}
                                <input name="filter[isFinishDate]" type="checkbox" style="margin-left: 0 !important;">���� �� �������
                            {% endif %}
                        </div>
                    </div>-->
                </th>
                <th class="column-order-th">
                    <div class="column-order-title">
                        ����. ���� ��������� �������
                    </div>
                </th>
                <th class="column-order-th">
                    <div class="column-order-title" style="width: 100px; !important;">
                        ���� ����-�������
                    </div>
                </th>
                <th class="column-order-th">
                    <div class="column-order-title" style="width: 100px; !important;">
                        ���� ������ ������
                    </div>
                </th>
                <!--<th class="column-order-th">
                    <div class="column-order-title">
                        ��. ���� ������ ���. ������
                        <div data-date-format="yyyy-mm-dd" data-date="" class="input-append date dp">
                            <input type="text" class="span6" style="font-size: 10px;" placeholder="���� ������" name="filter[planDateLineDepartment]" readonly="readonly" size="10" value="{{ filter.planDateLineDepartment }}">
                            <span class="add-on"><i class="icon-calendar"></i></span>
                            <span class="add-on"><i class="icon-trash"></i></span>
                        </div>
                    </div>
                    <div class="column-order-button">
                        <div class="checkbox" style="padding-left: 0;">
                            {% if isDateLineDepartment == 1 %}
                                <input name="filter[isDateLineDepartment]" type="checkbox" checked style="margin-left: 0 !important;">���� �� �������
                            {% else %}
                                <input name="filter[isDateLineDepartment]" type="checkbox" style="margin-left: 0 !important;">���� �� �������
                            {% endif %}
                        </div>
                    </div>
                </th>-->
                <th class=span2>
                    ����������� �����
                </th>
                <th class=span2>
                    ����.������.������
                </th>
                <th class="column-order-th">
                    <div class="column-order-title" style="width: 100px; !important;">
                        ����. ����� ��.
                    </div>
                    <div class="column-order-button">
                        <button class="btn btn-mini" name="filter[planDeviceSort]"  value="asc"><i class=" icon-arrow-up"></i></button>
                        <button class="btn btn-mini" name="filter[planDeviceSort]"  value="desc"><i class=" icon-arrow-down"></i></button>
                        <div>�����:</div>
                        <div>
                            {{ totalCost.totalPlanDeviceCost | number_format(2, ',',' ') }}
                        </div>
                    </div>
                </th>
                <th class="column-order-th">
                    <div class="column-order-title" style="width: 100px; !important;">
                        ����. ����� ��.
                    </div>
                    <div class="column-order-button">
                        <button class="btn btn-mini" name="filter[factDeviceSort]"  value="asc"><i class=" icon-arrow-up"></i></button>
                        <button class="btn btn-mini" name="filter[factDeviceSort]"  value="desc"><i class=" icon-arrow-down"></i></button>
                        <div>�����:</div>
                        <div>
                            {{ totalCost.totalFactDeviceCost | number_format(2, ',',' ') }}
                        </div>
                    </div>
                </th>
                <th class="column-order-th span7">
                    <div class="column-order-title" style="width: 100px; !important;">
                        ����. ����� ���
                    </div>
                    <div class="column-order-button">
                        <button class="btn btn-mini"  name="filter[planSmrSort]"  value="asc"><i class=" icon-arrow-up"></i></button>
                        <button class="btn btn-mini"  name="filter[planSmrSort]"  value="desc"><i class=" icon-arrow-down"></i></button>
                        <div>�����:</div>
                        <div>
                            {{ totalCost.totalPlanSmrCost | number_format(2, ',',' ') }}
                        </div>
                    </div>
                </th>
                <th class="column-order-th" >
                    <div class="column-order-title" style="width: 100px; !important;">
                        ����. ����� ���
                    </div>
                    <div class="column-order-button">
                        <button class="btn btn-mini" name="filter[factSmrSort]"  value="asc"><i class=" icon-arrow-up"></i></button>
                        <button class="btn btn-mini" name="filter[factSmrSort]"  value="desc"><i class=" icon-arrow-down"></i></button>
                        <div>�����:</div>
                        <div>
                            {{ totalCost.totalFactSmrCost | number_format(2, ',',' ') }}
                        </div>
                    </div>
                </th>
                <th class="column-order-th span7">
                    <div class="column-order-title" style="width: 100px; !important;">
                        Capex (���)
                    </div>
                    <div class="column-order-button">
                        <button class="btn btn-mini" name="filter[capexSort]"  value="asc"><i class=" icon-arrow-up"></i></button>
                        <button class="btn btn-mini" name="filter[capexSort]"  value="desc"><i class=" icon-arrow-down"></i></button>
                        <div>�����:</div>
                        <div>
                            {{ totalCost.capex | number_format(2, ',',' ') }}
                        </div>
                    </div>
                </th>
                <th class="column-order-th span7">
                    <div class="column-order-title" style="width: 100px; !important;">
                        Opex (���)
                    </div>
                    <div class="column-order-button">
                        <button class="btn btn-mini" name="filter[opexSort]"  value="asc"><i class=" icon-arrow-up"></i></button>
                        <button class="btn btn-mini" name="filter[opexSort]"  value="desc"><i class=" icon-arrow-down"></i></button>
                        <div>�����:</div>
                        <div>
                            {{ totalCost.opex | number_format(2, ',',' ') }}
                        </div>
                    </div>
                </th>
                <th class="column-order-th span7">
                    <div class="column-order-title" style="width: 100px; !important;">
                        ������ ���
                    </div>
                    <div class="column-order-button">
                        <button class="btn btn-mini" name="filter[deltaSmrSort]"  value="asc"><i class=" icon-arrow-up"></i></button>
                        <button class="btn btn-mini" name="filter[deltaSmrSort]"  value="desc"><i class=" icon-arrow-down"></i></button>
                        <div>�����:</div>
                        <div>
                            {{ totalCost.totalDeltaSmrCost | number_format(2, ',',' ') }}
                        </div>
                    </div>
                </th>
                <th class="column-order-th span7">
                    <div class="column-order-title" style="width: 100px; !important;">
                        ����. EBITDA (%)
                    </div>
                    <div class="column-order-button">
                        <button class="btn btn-mini" name="filter[ebitdaRentSort]"  value="asc"><i class=" icon-arrow-up"></i></button>
                        <button class="btn btn-mini" name="filter[ebitdaRentSort]"  value="desc"><i class=" icon-arrow-down"></i></button>
                    </div>
                </th>
                <th class="column-order-th span7">
                    <div class="column-order-title" style="width: 100px; !important;">
                        ��.����.����.(���)
                    </div>
                    <div class="column-order-button">
                        <button class="btn btn-mini" name="filter[paybackSort]"  value="asc"><i class=" icon-arrow-up"></i></button>
                        <button class="btn btn-mini" name="filter[paybackSort]"  value="desc"><i class=" icon-arrow-down"></i></button>
                    </div>
                </th>
                <th class="column-order-th span7">
                    <div class="column-order-title" style="width: 100px; !important;">
                        ����. EBITDA (%)
                    </div>
                    <div class="column-order-button">
                        <button class="btn btn-mini" name="filter[factEbitdaRentSort]"  value="asc"><i class=" icon-arrow-up"></i></button>
                        <button class="btn btn-mini" name="filter[factEbitdaRentSort]"  value="desc"><i class=" icon-arrow-down"></i></button>
                    </div>
                </th>
                <th class="column-order-th span7">
                    <div class="column-order-title" style="width: 100px; !important;">
                        ����.����.����.(���)
                    </div>
                    <div class="column-order-button">
                        <button class="btn btn-mini" name="filter[factPaybackSort]"  value="asc"><i class=" icon-arrow-up"></i></button>
                        <button class="btn btn-mini" name="filter[factPaybackSort]"  value="desc"><i class=" icon-arrow-down"></i></button>
                    </div>
                </th>
                <th  class="column-order-th ">
                    <div class="column-order-title">
                        ����� ������
                    </div>
                </th>
                <th  class="column-order-th ">
                    <div class="column-order-title">
                        <input type="text" id="seller" name="seller" placeholder="���������" class="span3" value="{{ seller }}" />
                        <input type="hidden" id="filterSellerId" name="filter[sellerId]"  value="{{ filter.sellerId }}" />
                    </div>
                </th>
            </tr>

            {% for plan in planCapitalCost %}
                <tr>
                    <!--{% if plan.teoId != 0 %}
                        <td class="span1"><a href="./index.php?mod=teo&act=add_proj&id={{ plan.teoId }}">{{ plan.teoId }}</a></td>
                    {% else %}
                        <td>{{ plan.teoId }}</td>
                    {% endif %}-->
                    <td>{{ plan.teoId }}</td>
                    {% if plan.teoId != 0 %}
                        <td class="span4"><a href="./index.php?mod=teo&act=add_proj&id={{ plan.teoId }}">{{ plan.teoProjectName }}</a></td>
                    {% else %}
                        <td>{{ plan.teoProjectName }}</td>
                    {% endif %}
                    {% if plan.teoId != 0 %}
                        <td class="span3"><a href="./index.php?mod=teo&act=add_proj&id={{ plan.teoId }}">{{ plan.address }}</a></td>
                    {% else %}
                        <td>{{ plan.address }}</td>
                    {% endif %}
                    <!--<td class="span2">{{ plan.manager}}</td>-->
                    <td>{{ plan.contract }}</td>
                    <td>{{ plan.brand }}</td>
                    <td class="span2">{{ plan.manager}}</td>
                    <td class="span2">
                        {% if plan.al == '1' %}
                            ����������� �����
                        {% endif %}

                        {% if plan.e1 == '1' %}
                            ����� �1
                        {% endif %}

                        {% if plan.int == '1' %}
                            ��������
                        {% endif %}

                        {% if plan.kpd == '1' %}
                            ���
                        {% endif %}
                    </td>
                    <td>
                        {{ plan.sphere }}
                    </td>
                    <!--<td class="span2">{{ plan.projectManager}}</td>-->
                    <!--<td class="span2">{{ plan.planServiceDate }}</td>-->
                    <td class="span2"><a href="./index.php?mod=ur&act=edit_task&type=view&task_id={{ plan.urId }}">{{ plan.planSmrDate }}</a></td>
                    <td class="span2"><a href="./index.php?mod=ur&act=edit_task&type=view&task_id={{ plan.urId }}">{{ plan.factSmrDate }}</a></td>
                    <td class="span2"><a href="./index.php?mod=smr&act=edit&smrId={{ plan.smrId }}">{{ plan.dateInvoice }}</a></td>
                    <td class="span2">{{ plan.startPayDate }}</td>
                    <td class="span2">{{ plan.planSumIncome | number_format(2, ',',' ')}}</td>
                    <td class="span2">{{ plan.averageSum | number_format(2, ',',' ')}}</td>
                    <!--<td class="span2"><a href="./index.php?mod=planConnect&action=edit&planConnectId={{ plan.planConnectId }}">{{ plan.dateExit }}</a></td>-->
                    <td>{{ plan.planDeviceCost | number_format(2, ',',' ') }}</td>
                    <td>
                        {% if plan.factDeviceCost != 0  %}
                            {{ plan.factDeviceCost | number_format(2, ',',' ') }}
                        {% else %}
                            0.0
                        {% endif %}
                    </td>
                    <td>{{ plan.planSmrCost | number_format(2, ',',' ') }}</td>
                    <td>
                        {% if plan.smrId %}
                            <a href="./index.php?mod=smr&act=edit&smrId={{ plan.smrId }}">{{ plan.factSmrCost | number_format(2, ',',' ') }}</a>
                        {% else %}
                            {{ plan.factSmrCost | number_format(2, ',',' ') }}
                        {% endif %}
                    </td>
                    <td>{{ plan.capex | number_format(2, ',',' ') }}</td>
                    <td>{{ plan.opex | number_format(2, ',',' ') }}</td>
                    <td>
                        {{ plan.deltaSmr | number_format(2, ',',' ') }}
                    </td>
                    <td>
                       {{  plan.planEbitdaRent | raw }}
                    </td>
                    <td>
                       {{  plan.planPayback }}
                    </td>
                     <td>
                       {{  plan.factEbitdaRent | raw }}
                    </td>
                    <td>
                       {{  plan.factPayback }}
                    </td>
                    <td>
                        {% if plan.quantity != null %}
                            {{  plan.quantity | number_format(2, ',',' ') }}
                        {% else %}
                            0,00
                        {% endif %}
                    </td>
                    <td>
                       {{  plan.seller }}
                    </td>
                </tr>
            {% endfor %}
        </table>

    </div>
</form>