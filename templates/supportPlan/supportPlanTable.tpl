{% if rights.planView %}
<script type="text/javascript">
    $(document).ready(function ()
    {
        //�������������� ��� ����������
        $('#filterContractNo').autocomplete
        ({
            serviceUrl: './modules/ajax/autocomplete.php', // �������� ��� ��������� �������� ��������������
            minChars: 2, // ����������� ����� ������� ��� ������������ ��������������
            maxHeight: 400, // ������������ ������ ������ ���������, � ��������
            width: 500, // ������ ������
            maxWidth: 800, // ������ ������
            zIndex: 9999, // z-index ������
            deferRequestBy: 200, // �������� ������� (����), �� ������, ���� �� �� ����� ����� ������� ��������, ���� ������������ ��������. � ������ ������ 300.
            params: {type: 'contractsList'}, // �������������� ���������
            formatResult: contractsFormatResult,
            onSelect: function (data)
            {
                $('#filterContractNo').val(data.value);
                $('#filterOrderId').val(data.data); // ��� ����� � �������� ��������� ����������� � ����.
            }// Callback �������, ������������� �� ����� ������ �� ������������ ���������,
        });

        //������������ ��� ����������
        $('#addContractNo').autocomplete
        ({
            serviceUrl: './modules/ajax/autocomplete.php', // �������� ��� ��������� �������� ��������������
            minChars: 1, // ����������� ����� ������� ��� ������������ ��������������
            maxHeight: 400, // ������������ ������ ������ ���������, � ��������
            width: 500, // ������ ������
            maxWidth: 800, // ������ ������
            zIndex: 9999, // z-index ������
            deferRequestBy: 200, // �������� ������� (����), �� ������, ���� �� �� ����� ����� ������� ��������, ���� ������������ ��������. � ������ ������ 300.
            params: {type: 'contractsList'}, // �������������� ���������
            formatResult: contractsFormatResult,
            onSelect: function (data)
            {
                $('#addContractNo').val(data.value);
                $('#addOrderId').val(data.data); // ��� ����� � �������� ��������� ����������� � ����.
            }// Callback �������, ������������� �� ����� ������ �� ������������ ���������,
        });

        $('#filterAddress').autocomplete(
        {
            serviceUrl: './modules/ajax/autocomplete.php', // �������� ��� ��������� �������� ��������������
            minChars: 2, // ����������� ����� ������� ��� ������������ ��������������
            maxHeight: 400, // ������������ ������ ������ ���������, � ��������
            width: 500, // ������ ������
            maxWidth: 800, // ������ ������
            zIndex: 9999, // z-index ������
            deferRequestBy: 200, // �������� ������� (����), �� ������, ���� �� �� ����� ����� ������� ��������, ���� ������������ ��������. � ������ ������ 300.
            params: {type: 'fstAddressList'}, // �������������� ���������
            onSelect: function (data)
            {
                $('#filterAddress').val(data.value);
                $('#filterHouseId').val(data.data);
            } // Callback �������, ������������� �� ����� ������ �� ������������ ���������,
        });

        $('#addAddress').autocomplete(
        {
            serviceUrl: './modules/ajax/autocomplete.php', // �������� ��� ��������� �������� ��������������
            minChars: 2, // ����������� ����� ������� ��� ������������ ��������������
            maxHeight: 400, // ������������ ������ ������ ���������, � ��������
            width: 500, // ������ ������
            maxWidth: 800, // ������ ������
            zIndex: 9999, // z-index ������
            deferRequestBy: 200, // �������� ������� (����), �� ������, ���� �� �� ����� ����� ������� ��������, ���� ������������ ��������. � ������ ������ 300.
            params: {type: 'fstAddressList'}, // �������������� ���������
            onSelect: function (data)
            {
                $('#addAddress').val(data.value);
                $('#addHouseId').val(data.data);
            } // Callback �������, ������������� �� ����� ������ �� ������������ ���������,
        });

        $('#user1').autocomplete({
            serviceUrl: './modules/ajax/autocomplete.php', // �������� ��� ��������� �������� ��������������
            minChars: 2, // ����������� ����� ������� ��� ������������ ��������������
            maxHeight: 400, // ������������ ������ ������ ���������, � ��������
            width: 500, // ������ ������
            maxWidth: 800, // ������ ������
            zIndex: 9999, // z-index ������
            deferRequestBy: 200, // �������� ������� (����), �� ������, ���� �� �� ����� ����� ������� ��������, ���� ������������ ��������. � ������ ������ 300.
            params: {type: 'users_list'}, // �������������� ���������
            //formatResult: userFormatResult,
            onSelect: function (data)
            {
                $('#user1').val(data.value);
            }
        });

        $('#user2').autocomplete({
            serviceUrl: './modules/ajax/autocomplete.php', // �������� ��� ��������� �������� ��������������
            minChars: 2, // ����������� ����� ������� ��� ������������ ��������������
            maxHeight: 400, // ������������ ������ ������ ���������, � ��������
            width: 500, // ������ ������
            maxWidth: 800, // ������ ������
            zIndex: 9999, // z-index ������
            deferRequestBy: 200, // �������� ������� (����), �� ������, ���� �� �� ����� ����� ������� ��������, ���� ������������ ��������. � ������ ������ 300.
            params: {type: 'users_list'}, // �������������� ���������
            //formatResult: userFormatResult,
            onSelect: function (data)
            {
                $('#user2').val(data.value);
            }
        });

        $('#author').autocomplete({
            serviceUrl: './modules/ajax/autocomplete.php', // �������� ��� ��������� �������� ��������������
            minChars: 2, // ����������� ����� ������� ��� ������������ ��������������
            maxHeight: 400, // ������������ ������ ������ ���������, � ��������
            width: 500, // ������ ������
            maxWidth: 800, // ������ ������
            zIndex: 9999, // z-index ������
            deferRequestBy: 200, // �������� ������� (����), �� ������, ���� �� �� ����� ����� ������� ��������, ���� ������������ ��������. � ������ ������ 300.
            params: {type: 'users_list'}, // �������������� ���������
            onSelect: function (data)
            {
                $('#author').val(data.value);
                $('#authorId').val(data.data)

            } // Callback �������, ������������� �� ����� ������ �� ������������ ���������,
        });
    });

    // � ���������� ���� �������������� ������ ����������� ������� ���� � �� ������� ��������� � ������� ��� ����
    function editTeam()
    {
        $.ajax({
            url: './index.php?mod=support&act=getTeamForEdit',
            type: 'POST',
            dataType: 'json',
            success: function (data)
            {
                if(!$('tr').is('[id ^= teamEditId]'))
                {
                    var i;
                    for (i = 0; i <= data.length; i++)
                    {
                        $('#teamEdit').prepend('<tr id="teamEditId' + data[i]['teamId'] + '"><td>�������</td><td><input class="span9" type="text" readonly value="' + data[i]['team'] + '"/>&nbsp;&nbsp;' +
                                '<a class="btn" data-toggle="modal" onclick="addTeamInBan(' + data[i]['teamId'] + ');return false;"><i class="ico_sprite ico_user_delete"></i>&nbsp;�������</a></td></tr>');
                    }
                }
            }
        });
    }

    // ��������� ������� � ���
    function addTeamInBan(teamId)
    {
        $.ajax({
            url: './index.php?mod=support&act=addTeamInBan',
            type: 'POST',
            dataType: 'json',
            data: {teamId:teamId},
            success: function (data)
            {
                $('#teamEditId'+teamId).remove();
            }
        });
    }

    // ��������� � ���� ����� �������
    function addNewTeam()
    {
        var user1 =$('#user1').val();
        var user2 =$('#user2').val();
        var team = '';

        (user1 != '' && user2 != '') ? team = user1 + ', ' + user2 : '';

        (user1 != '' && user2 == '') ? team = user1 : '';

        (user1 == '' && user2 != '')? team = user2 : '';

        if (!team == '')
        {
            $.ajax({
                url: './index.php?mod=support&act=addNewTeam',
                type: 'POST',
                dataType: 'json',
                data: {newTeam: team},
                success: function ()
                {
                    send_error('','����� ������� ������� ���������');
                    setTimeout(function ()
                    {
                        location.reload();
                    }, 800);
                }
            });
        }
        else
        {
            alert('���� ���������� ������� �� ���������!');
        }
    }

</script>
<div class="navigation" style="text-align:center;">{{ pages | raw }}</div>

<br />

<form method="GET" action="./index.php?mod=support">

    <a class="btn btn-success" href="#createPlan" data-toggle="modal">������������� �����</a>
    <input type="hidden" name="mod" value="support" />
    <input type="submit" value="������" class="btn btn-info" />
    <a href="./index.php?mod=support" class="btn btn-info">�����</a>
    {% if rights.planEdit %}
        <a href="{{ csv }}" class="btn bhelp" name="csv" data-original-title="������� � CSV � ������ �������" data-placement="right"><i class="ico_sprite ico_excel"></i></a>
        <a class="btn btn-warning pull-right" href="#delTeam" data-toggle="modal" onclick="editTeam();return false;">������� �������</a>
        <a class="btn btn-success pull-right" href="#addTeam" data-toggle="modal" onclick="editTeam();return false;">�������� �������</a>
    {% endif %}
    <br />
    <br />

    <table class="table table-bordered">
        <thead class="vtop">
            <tr>
                <th>
                    �����
                </th>
                <th>
                    <input type="text" id="filterContractNo" name="filterContract" placeholder="� ���" class="span1" value="{{ filterContract }}" />
                    <input type="hidden" id="filterOrderId" name="filter[orderId]"  value="{{ filter.orderId  }}" />
                </th>
                <th>
                    <input type="text" id="contract" name="filter[ticketId]" placeholder="� ������" class="span1" value="{{ filter.ticketId }}" />
                </th>
                <th>
                    <input type="text" name="filter[clientName]" placeholder="�������� �������" class="span2" value="{{ filter.clientName }}" />
                </th>
                <th style="position: relative;">
                    <input type="text" id="filterAddress" name="filterAddress" class="span2" placeholder="�����" value="{{ filterAddress }}" />
                    <input type="hidden" id="filterHouseId" name="filter[houseId]"  value="" />
                </th>
                <th style="position: relative;">
                    <div data-date-format="yyyy-mm-dd" data-date="" class="input-append date dp">
                        <input type="text" style="font-size: 10px;" placeholder="����� �" name="filter[dateFrom]" readonly="readonly" size="10" class="span1" value="{{ filter.dateFrom }}">
                        <span class="add-on"><i class="icon-calendar"></i></span>
                        <span class="add-on"><i class="icon-trash"></i></span>
                    </div>
                    <div data-date-format="yyyy-mm-dd" data-date="" class="input-append date dp">
                        <input type="text" style="font-size: 10px;" placeholder="����� ��" name="filter[dateTo]" readonly="readonly" size="10" class="span1" value="{{ filter.dateTo }}">
                        <span class="add-on"><i class="icon-calendar"></i></span>
                        <span class="add-on"><i class="icon-trash"></i></span>
                    </div>
                </th>
                <th>
                    ������ ����������
                    <select name="filter[workStatusId]" class="span2" multiple="multiple" size="6" style="font-size: 11px; overflow: auto;">
                        {% if not filter.workStatusId %}
                            <option value="" selected="selected"></option>
                        {% endif %}
                        {% for  value in workStatus %}
                            {% if filter.workStatusId == value.statusId %}
                                <option value="{{ filter.workStatusId  }}" selected="selected">{{ value.description }}</option>
                            {% else %}
                                <option value="{{ value.statusId }}">{{ value.description }}</option>
                            {% endif %}
                        {% endfor %}
                    </select>
                </th>
                <th>
                    �����������<br>
                    <select  name="filter[overtimeStatusId]" class="span1" multiple="multiple" size="3" style="font-size: 11px; overflow: hidden;">
                        {% if not filter.overtimeStatusId %}
                            <option value="" selected="selected"></option>
                        {% endif %}
                        {% for value in overtimeStatus %}
                            {% if filter.overtimeStatusId == value.overtimeStatusId %}
                                <option value="{{ filter.overtimeStatusId }}" selected="selected">{{ value.description }}</option>
                            {% else %}
                                <option value="{{ value.overtimeStatusId }}">{{ value.description }}</option>
                            {% endif %}
                        {% endfor %}
                    </select>
                </th>
                <th>
                    <input type="text" id="author" name="filterAuthor" placeholder="������" class="_span2" value="{{ filterAuthor }}" />
                    <input type="hidden" id="authorId" name="filter[authorId]" value="{{ filter.authorId }}" />
                </th>
                <th style="position: relative;">
                    ������������� �������<br>
                    <select  name="filter[teamId]" class="span4" multiple="multiple" size="5" style="font-size: 11px;">
                        {% if not filter.teamId %}
                            <option value="" selected="selected"></option>
                        {% endif %}
                        {% for value in team %}
                            {% if filter.teamId == value.teamId %}
                                <option value="{{ filter.teamId }}" selected="selected">({{ value.team }})</option>
                            {% else %}
                                <option value="{{ value.teamId }}">({{ value.team }})</option>
                            {% endif %}
                        {% endfor %}
                    </select>
                </th>
                <th>
                    ��������<br>
                     {% if driverFilter == 1 %}
                         <input name="filter[driverFilter]" type="checkbox" checked>�� �����������
                     {% else %}
                         <input name="filter[driverFilter]" type="checkbox">�� �����������
                     {% endif %}
                     <select  name="filter[driverId]" class="span3" multiple="multiple" size="6" style="font-size: 11px; overflow: auto;">
                        {% for value in driver %}
                            {% if filter.driverId == value.userId %}
                                <option value="{{ filter.driverId }}" selected="selected">{{ value.driverName }}</option>
                            {% else %}
                                <option value="{{ value.userId }}">{{ value.driverName }}</option>
                            {% endif %}
                        {% endfor %}
                    </select>
                </th>
                <th style="min-width: 270px;">
                    ��������
                </th>
                {% if planConnectAllEdit %}
                    <th>��.</th>
                {% endif %}
            </tr>
        </thead>

        {% for plan in supportPlan %}
            <tr style="background-color: {{ connect.color }};">
                <td><a href="./index.php?mod=schema&act=view&amp;id={{ plan.orderId }}"><i class="ico_sprite ico_tables"></i></a></td>
                <td>
                    <a href="./index.php?mod=support&act=getPlanById&planId={{ plan.planId }}" >
                        {{ plan.contractNo }}
                    </a>
                    <input type="hidden" name="planId" value="{{ plan.planId }}">
                </td>
                <td>{{ plan.ticketId }}</td>
                <td>{{ plan.clientName | raw }}</td>
                <td>{{ plan.city }}, {{ plan.street}}, {{ plan.house }}</td>
                <td>{{ plan.planDatetime }}</td>
                <td>{{ plan.statusDescription }}</td>
                <td>{{ plan.overtimeDescription }}</td>
                <td>{{ plan.fullname }}</td>
                <td>{{ plan.teamName }}</td>
                <td>
                    {% if  plan.driverName == null %}
                        ��� ��������
                    {% else %}
                         {{ plan.driverName }}
                    {% endif %}
                </td>
                <td>{{ plan.description }}</td>
                {% if planConnectAllEdit %}
                    <td><a href="./index.php?mod=planConnect&action=delete&planConnectId={{ connect.planConnectId }}" onclick="if(!confirm('������� �����?')) return false;"><i class="ico_sprite ico_down"></i></a></td>
                {% endif %}
            </tr>
        {% endfor %}
    </table>

</form>
<!--����������� ������� � �������� ��� ���������� ������-->
{% include 'supportPlan/supportPlanAdd.tpl' %}
{% include 'supportPlan/supportPlanDeleteTeam.tpl' %}
{% include 'supportPlan/supportPlanAddTeam.tpl' %}
{% endif %}