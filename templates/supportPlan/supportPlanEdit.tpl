{% if rights.planView %}
<script type="text/javascript">
    $(document).ready(function ()
    {
        $('.contractNo').autocomplete
        ({
            serviceUrl: './modules/ajax/autocomplete.php', // �������� ��� ��������� �������� ��������������
            minChars: 2, // ����������� ����� ������� ��� ������������ ��������������
            //delimiter: /(,|;)\s*/, // ����������� ��� ���������� ��������, ������ ��� ���������� ���������
            maxHeight: 400, // ������������ ������ ������ ���������, � ��������
            width: 500, // ������ ������
            maxWidth: 800, // ������ ������
            zIndex: 9999, // z-index ������
            deferRequestBy: 200, // �������� ������� (����), �� ������, ���� �� �� ����� ����� ������� ��������, ���� ������������ ��������. � ������ ������ 300.
            params: {type: 'contractsList'}, // �������������� ���������
            //preserveInput: true,
            //triggerSelectOnValidInput: false,
            formatResult: contractsFormatResult,
            onSelect: function (data)
            {
                $('.contractNo').val(data.value);
                $('#orderId').val(data.data); // ��� ����� � �������� ��������� ����������� � ����.
                $('#editOrderId').val(data.data); // ��� ����� � �������� ��������� ����������� � ����.
            }// Callback �������, ������������� �� ����� ������ �� ������������ ���������,
        });

        $('.address').autocomplete(
        {
            serviceUrl: './modules/ajax/autocomplete.php', // �������� ��� ��������� �������� ��������������
            minChars: 2, // ����������� ����� ������� ��� ������������ ��������������
            //delimiter: /(,|;)\s*/, // ����������� ��� ���������� ��������, ������ ��� ���������� ���������
            maxHeight: 400, // ������������ ������ ������ ���������, � ��������
            width: 500, // ������ ������
            maxWidth: 800, // ������ ������
            zIndex: 9999, // z-index ������
            deferRequestBy: 200, // �������� ������� (����), �� ������, ���� �� �� ����� ����� ������� ��������, ���� ������������ ��������. � ������ ������ 300.
            params: {type: 'fstAddressList'}, // �������������� ���������
            onSelect: function (data)
            {
                $('.address').val(data.value);
                $('#editHouseId').val(data.data);
            } // Callback �������, ������������� �� ����� ������ �� ������������ ���������,
        });

        $('#newDriver').autocomplete({
            serviceUrl: './modules/ajax/autocomplete.php', // �������� ��� ��������� �������� ��������������
            minChars: 2, // ����������� ����� ������� ��� ������������ ��������������
            //delimiter: /(,|;)\s*/, // ����������� ��� ���������� ��������, ������ ��� ���������� ���������
            maxHeight: 400, // ������������ ������ ������ ���������, � ��������
            width: 500, // ������ ������
            maxWidth: 800, // ������ ������
            zIndex: 9999, // z-index ������
            deferRequestBy: 200, // �������� ������� (����), �� ������, ���� �� �� ����� ����� ������� ��������, ���� ������������ ��������. � ������ ������ 300.
            params: {type: 'driver_list'}, // �������������� ���������
            //formatResult: userFormatResult,
            onSelect: function (data)
            {
                console.log(data);
                $('#newDriver').val(data.value);
                $('#newDriverId').val(data.data)

            } // Callback �������, ������������� �� ����� ������ �� ������������ ���������,
        });


        $('#comment').keypress(function(){
            $('#colChar').text("��������: " + (2000 - ($('#comment').val().length)) + " ��������");
        });
    });

    $(function() {
        $( "#editTime" ).timepicker();
        $( "#editDate" ).datepicker();
  });

    function addComment()
    {
        if($('#comment').val().length <= 2000)
        {
            $('#addCommentForm').submit();
        }
        else
        {
            alert('����� ������ �� ������ ��������� 2000 ��������');
        }
    }
</script>
<div class="row-fluid">
    <div class="span6">
        <form action="./index.php?mod=support&act=editPlan" method="post">
            <table class="table table-bordered">
                <tr>
                    <td>
                        ����� ��������
                    </td>
                    <td>
                        <input type="text" id="editContractNo" name="contractNo"  class="contractNo" value="{{ planById.contractNo }}" required/>
                        <input type="hidden" id="editOrderId"  name="supportPlanEdit[orderId]" value="{{ planById.orderId }}"/>
                        <input type="hidden" id="editPlanId"  name="supportPlanEdit[planId]" value="{{ planById.planId }}"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        ����� ������
                    </td>
                    <td>
                        <input type="text" id="editTicketId" name="supportPlanEdit[ticketId]"  placeholder="123456/16" value="{{ planById.ticketId }}" required/>
                    </td>
                </tr>
                <tr>
                    <td>
                        �����
                    </td>
                    <td>
                        <input type="text" id="editAddress" name="editAddress" class="address span12" value="{{ planById.city }}, {{ planById.street }}, {{ planById.house }}" required/>
                        <input type="hidden" id="editHouseId" name="supportPlanEdit[houseId]" value="{{ planById.houseId }}"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        ���� � ����� ������
                    </td>
                    <td>
                        <div data-date-format="yyyy-mm-dd" data-date class="input-append date dp span3">
                            <input id="editDate" type="text" class="span8"  name="supportPlanEdit[planDate]" size="16" value="{{ planById.planDatetime[:10] }}" required>
                            <span class="add-on"><i class="icon-calendar"></i></span>
                            <span class="add-on"><i class="icon-trash"></i></span>
                        </div>
                        <div class="span3">
                            <input class="span7" id="editTime" type="text"  name="supportPlanEdit[planTime]" size="10"  value="{{ planById.planDatetime[11:15] }}" required>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        ������ ����������
                    </td>
                    <td>
                        <select id="editWorkStatusId" name="supportPlanEdit[workStatusId]" class="span11" required>
                            {% for ws in workStatus %}
                                {% if  ws.statusId ==  planById.statusId %}
                                    <option value="{{ planById.statusId }}" selected="selected">{{  planById.statusDescription }}</option>
                                {% else %}
                                    <option value="{{ ws.statusId }}">{{ ws.description }}</option>
                                {% endif %}
                            {% endfor %}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        ������������� �������
                    </td>
                    <td>
                        <select id="editteamId" name="supportPlanEdit[teamId]" class="span11" required>
                            {% for e in team %}
                                {% if  e.teamId ==  planById.teamId %}
                                    <option value="{{ planById.teamId }}" selected="selected">{{  planById.teamName }}</option>
                                {% else %}
                                    <option value="{{ e.teamId }}">{{ e.team }}</option>
                                {% endif %}
                            {% endfor %}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        ��������
                    </td>
                    <td>
                        <select id="editDriverId" name="supportPlanEdit[driverId]" class="span11">
                            {% for d in driver %}
                                {% if  d.userId ==  planById.driverId %}
                                    {% if planById.driverId == null and planById.driverName == null %}
                                            <option value="" selected="selected">��� ��������</option>
                                        {% else %}
                                            <option value="{{ planById.driverId }}" selected="selected">{{  planById.driverName }}</option>
                                    {% endif %}
                                {% else %}
                                    <option value="{{ d.userId }}">{{ d.driverName }}</option>
                                {% endif %}
                            {% endfor %}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        �����������
                    </td>
                    <td>
                        <select id="editConfStatusId" name="supportPlanEdit[overtimeStatusId]" class="span11" required>
                            {% for c in overtimeStatus %}
                                {% if  c.overtimeStatusId ==  planById.overtimeDeviceId %}
                                    <option value="{{ planById.overtimeDeviceId }}" selected="selected">{{  planById.overtimeDescription }}</option>
                                {% else %}
                                    <option value="{{ c.overtimeStatusId }}">{{ c.description }}</option>
                                {% endif %}
                            {% endfor %}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        ������� ��������
                    </td>
                    <td>
                        <textarea id="editDescription" name="description" class="span12" rows="4" style="resize: none">{{  planById.description }}</textarea>
                    </td>

                </tr>
            </table>
            {% if rights.planEdit %}
                <button class="btn btn-success" type="submit">���������</button>
            {% endif %}
            <a href="./index.php?mod=support" class="btn btn-primary">� ������ �������</a><br/><br/>
        </form>
    </div>
</div>

<a class="btn indent-bottom" data-toggle="modal" href="#addComment"><i class="ico_sprite ico_add"></i>�������� �����������</a>

<div id="addComment" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="commentLabel" aria-hidden="true">
    <form id="addCommentForm" name="createComment" action="./index.php?mod=support&act=addComment" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 id="commentLabel">���������� �����������</h3>
        </div>
        <div class="modal-body">
            <div class="row-fluid">
                <div>
                    <span id="colChar" style="color: #8c8c8c;">��������: 2000 ��������</span>
                </div>
                <textarea class="textarea1" id="comment" name="comment" placeholder="������� ����� ����������� �� ����� 2000 ��������"></textarea>
                <input type="hidden"  name="commentPlanId" value="{{ planById.planId }}">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-success" onclick="addComment();return false;">�����������</button>
            <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">��������</button>
        </div>
    </form>
</div>
{{ CWidgetCommentList.render() }}
{% endif %}