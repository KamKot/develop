@extends('layouts.app')
@section('viewFeedback')
    <br/>
    {!! Breadcrumb::render() !!}
    <div class="row-fluid">
        @include('feedback.feedbackError')
        <div style="background-color:#e1e1ee; !important; border-radius: 6px;">
            <div class="col-lg-6">
                <form method="post" action="./editFeedback">
                    <table class="table">
                        <tr>
                            <td>{{ $feedbackView['title'] }}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Приоритет</td>
                            <td>
                                @foreach($priorityList as $list)
                                    <a class="btn btn-xs {{ $list['class'] }}" href="./changePriority/{{ $list['value'] }}">{{ $list ['name'] }}</a>
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <td>Создана</td>
                            <td>
                                {{ $feedbackView['open_date'] }}
                            </td>
                        </tr>
                        <tr>
                            <td>Отдел</td>
                            <td>
                                @foreach($departmentList as $list)
                                    @if($list['id'] == $feedbackView['department_id'])
                                        {{ $list['name'] }}
                                    @endif
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <td>Тип обращения</td>
                            <td>
                                <select class="col-lg-12 input-sm" style="background-color: {{ $feedbackView['category']['color'] }}" name="feedbackCategory">
                                    @foreach($categoryList as $list)
                                        @if($feedbackView['category']['id'] == $list->id)
                                            <option style="background-color: {{ $list->color }}" value="{{ $list->id }}" selected="selected">{{ $list->description }}</option>
                                        @else
                                            <option style="background-color: {{ $list->color }}" value="{{ $list->id }}">{{ $list->description }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Статус</td>
                            <td>
                                <select class="col-lg-12 input-sm" name="feedbackStatus">
                                    @foreach($statusList as $list)
                                        @if($feedbackView['status']['id'] == $list['id'])
                                            <option value="{{ $list['id'] }}" selected="selected">{{ $list['description'] }}</option>
                                        @else
                                            <option value="{{ $list['id'] }}">{{ $list['description'] }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Ответственный</td>
                            <td>
                                <input type="text" id="executingUserInput" class="form-control" value="{{ $feedbackView['executing_user']['fullname'] }}" />
                                <input type="hidden" id="executingUserId" name="executingUserId" value="{{ $feedbackView['executing_user_id'] }}" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input type="submit" class="btn btn-sm btn-info" value="Сохранить" />
                            </td>
                        </tr>
                    </table>
                    <div class="col-lg-12 col-lg-offset-0">
                        <h5>Описание задачи:</h5>
                        <p class="">{{ $feedbackView['description'] }}</p>
                    </div>
                    <input type="hidden" name="feedbackId" value="{{ $feedbackView['id'] }}">
                    {{ csrf_field() }}
                </form>
            </div>
            <div class="col-lg-6">
                <div class="col-lg-12">
                    <div>
                        <div class="col-lg-4 col-lg-offset-4">
                            <div class="userPhoto">
                                <a href="/index.php?mod=employees&act=view&userId={{ $feedbackView['author_id'] }}" ><img src="./upload/photo/w90_{{ $feedbackView['author']['photo'] }}" /></a>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="userContent">
                                <h4><a href="./index.php?mod=employees&act=view&userId={{ $feedbackView['author']['id'] }}" >{{ $feedbackView['author']['fullname'] }}</a></h4>
                                <h5>{{ $feedbackView['author']['position'] }}</h5>
                                <div><span style="color: #6d808f;">E-Mail:</span> <a href="mailto:{{ $feedbackView['author']['email'] }}">{{ $feedbackView['author']['email'] }}</a></div>
                                <div><span style="color: #6d808f;">Мобильный:</span> {{ $feedbackView['author']['mobile'] }}</div>
                                <div><span style="color: #6d808f;">Подразделение:</span> <a href="./index.php?mod=employees&searchDepartment={{ $feedbackView['author']['department_id'] }}" >{{ $feedbackView['author']['department_id'] }}</a></div>
                                <div><span style="color: #6d808f;">Внутренний телефон:</span> {{ $feedbackView['author']['phone'] }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function()
        {
            $('#executingUserInput').autocomplete(
            {
                serviceUrl: '/autocomplete/get-user',
                minChars: 2,
                maxHeight: 400,
                width: 500,
                maxWidth: 800,
                zIndex: 9999,
                deferRequestBy: 0,
                formatResult: userFormatResult,
                onSelect: function (data)
                {
                    $('#executingUserId').val(data.data);
                }
            });

            $('#followerUserInput').autocomplete(
            {
                serviceUrl: '/autocomplete/get-user',
                minChars: 2,
                maxHeight: 400,
                width: 500,
                maxWidth: 800,
                zIndex: 9999,
                deferRequestBy: 0,
                formatResult: userFormatResult,
                onSelect: function (data)
                {
                    $('#followerUserId').val(data.data);
                }
            });
        });
    </script>
@endsection