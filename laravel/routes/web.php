<?php
/**
 * Автокомплит
 */
Route::group(['middleware' => ['web', 'auth']], function ()
{
    Route::get('autocomplete/get-user', 'AutocompleteController@getUser');
});

Route::group(['middleware' => ['web', 'auth'], 'namespace' => 'Frontend'], function ()
{
    Route::get('/', 'DummyController@dummy');
});

/**
 * Обратная связь
 */
Route::group(['prefix' => 'feedback', 'middleware' => ['web', 'auth']],function ()
{
    Route::get('/', 'FeedbackController@index');
    Route::post('/', 'FeedbackController@index');
    Route::get('viewFeedback', 'FeedbackController@viewFeedback');
    Route::post('addFeedback', 'FeedbackController@addFeedback');
    Route::get('getCategoryListByDepartmentId/{id}', 'FeedbackController@getCategoryListByDepartmentId');
    Route::post('editFeedback', 'FeedbackController@editFeedback');
});
