<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FeedbackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * Категории задач в зависимости от отдела
         */
        Schema::create('feedback_category', function (Blueprint $table)
        {
             $table->increments('id');
             $table->string('description', 50)->comment("Название категории");
             $table->string('color', 7)->comment("Цвет");
        });

        /**
         * Статусы задач и подзадач
         */
        Schema::create('feedback_status', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('description',50)->comment("Описание статуса задачи");

            $table->index('id');
        });

        /**
         * Задачи
         */
        Schema::create('feedback', function (Blueprint $table)
        {
            $table->increments('id');
            $table->unsignedInteger('department_id')->comment("ИД департамента");
            $table->unsignedInteger('feedback_category_id')->comment("ИД категории задачи");
            $table->unsignedInteger('feedback_status_id')->default(1)->comment("ИД статуса");
            $table->unsignedInteger('author_id')->comment("ИД пользователя, создавшего задачу");
            $table->unsignedInteger('executing_user_id')->nullable()->comment("ИД ответственного пользователя");
            $table->enum('priority',['low','middle','high'])->default('low')->comment("Приоритет задачи");
            $table->string('title', 120)->nullable()->comment("Заголовок задачи");
            $table->text('description')->nullable()->comment("Описание задачи");
            $table->dateTime('open_date')->comment("Дата и время создания задачи");
            $table->dateTime('close_date')->nullable()->comment("Дата и время закрытия задачи");

            $table->index('department_id');
            $table->index('feedback_category_id');
            $table->index('feedback_status_id');
            $table->index('author_id');
            $table->index('executing_user_id');

            $table->foreign('department_id')->references('id')->on('department');
            $table->foreign('feedback_category_id')->references('id')->on('feedback_category');
            $table->foreign('feedback_status_id')->references('id')->on('feedback_status');
            $table->foreign('author_id')->references('id')->on('user');
            $table->foreign('executing_user_id')->references('id')->on('user');
        });


        /**
         * Комментарии к задачам
         */
        Schema::create('feedback_comment', function (Blueprint $table)
        {
             $table->increments('id');
             $table->unsignedInteger('feedback_id')->comment("ИД задачи");
             $table->unsignedInteger('user_id')->comment("ИД пользователя добавившего комментарий");
             $table->dateTime('add_date')->comment("Дата и время создания комментария");
             $table->string('comment', 2000)->nullable()->comment("Текст комментария");

             $table->index('feedback_id');
             $table->index('user_id');

             $table->foreign('feedback_id')->references('id')->on('feedback');
             $table->foreign('user_id')->references('id')->on('user');
        });

        /**
         * Связующая таблица для связи многие-ко-многим
         */
        Schema::create('feedback_department_category', function (Blueprint $table)
        {
            $table->unsignedInteger('department_id');
            $table->unsignedInteger('feedback_category_id');

            $table->index('department_id');
            $table->index('feedback_category_id');

            $table->foreign('department_id')->references('id')->on('department');
            $table->foreign('feedback_category_id')->references('id')->on('feedback_category');
        });

        /**
         * Файлы, прикрепленные к задачам
         */
        Schema::create('feedback_file', function (Blueprint $table)
        {
            $table->increments('id');
            $table->unsignedInteger('feedback_id')->nullable();
            $table->unsignedInteger('user_id')->nullable()->comment("ИД пользователя, добавившего файл");
            $table->string('file_description', 255)->nullable()->comment("Описание файла");
            $table->dateTime('add_date')->nullable()->comment("Дата и время добавление файла");

            $table->index('feedback_id');
            $table->index('user_id');

            $table->foreign('feedback_id')->references('id')->on('feedback');
            $table->foreign('user_id')->references('id')->on('user');
        });

        /**
         * Пользователи, принимающие участие в решении задачи(наблюдатели)
         */
        Schema::create('feedback_follower', function (Blueprint $table)
        {
            $table->unsignedInteger('feedback_id');
            $table->unsignedInteger('user_id')->comment("ИД пользователя, принимающего участие в задаче");
            $table->dateTime('add_date')->nullable()->comment("Дата и время добавление файла");

            $table->unique(['feedback_id','user_id']);

            $table->foreign('feedback_id')->references('id')->on('feedback');
            $table->foreign('user_id')->references('id')->on('user');
        });

        /**
         * Подзадачи для задач
         */
        Schema::create('feedback_subtask', function (Blueprint $table)
        {
            $table->increments('id');
            $table->unsignedInteger('feedback_id');
            $table->unsignedInteger('executing_user_id')->nullable()->comment("ИД ответственного пользователя");
            $table->unsignedInteger('feedback_status_id')->default(1)->comment("ИД статуса");
            $table->unsignedInteger('author_id')->comment("ИД пользователя, создавшего подзадачу");
            $table->unsignedInteger('close_user_id')->comment("ИД пользователя, выполнившего подзадачу");
            $table->text('description')->nullable()->comment("Описание подзадачи");
            $table->dateTime('open_date')->comment("Дата и время создания подзадачи");
            $table->dateTime('close_date')->nullable()->comment("Дата и время закрытия подзадачи");

            $table->index('feedback_id');
            $table->index('executing_user_id');
            $table->index('feedback_status_id');
            $table->index('author_id');
            $table->index('close_user_id');

            $table->foreign('feedback_id')->references('id')->on('feedback');
            $table->foreign('feedback_status_id')->references('id')->on('feedback_status');
            $table->foreign('author_id')->references('id')->on('user');
            $table->foreign('close_user_id')->references('id')->on('user');
            $table->foreign('executing_user_id')->references('id')->on('user');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feedback');
    }
}
