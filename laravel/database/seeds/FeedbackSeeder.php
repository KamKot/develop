<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class FeedbackSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('feedback_status')->insert([
            ['description' => 'Ожидает рассмотрения'],
            ['description' => 'Рассматривается'],
            ['description' => 'Выполняется'],
            ['description' => 'Выполнено'],
            ['description' => 'Отклонено'],
            ['description' => 'Отложено'],
            ['description' => 'Приостановлено'],
            ['description' => 'Невозможно выполнить'],
            ['description' => 'Тестирование'],
                                             ]);

        DB::table('feedback_category')->insert([
            ['description' => 'Перенос рабочего места', 'color' => '#DBFAC7'],
            ['description' => 'Установка программного обеспечения', 'color' => '#DBFAC7'],
            ['description' => 'Детализация', 'color' => '#DBFAC7'],
            ['description' => 'Проблема с рабочей станцией', 'color' => '#FAB8B8'],
            ['description' => 'Проблема с программным обеспечением', 'color' => '#FAB8B8'],
            ['description' => 'Проблема с переферийной техникой', 'color' => '#FAB8B8'],
            ['description' => 'Подготовка рабочего места', 'color' => '#DBFAC7'],
            ['description' => 'Консультация', 'color' => '#DBFAC7'],
            ['description' => 'Другое', 'color' => '#EEEEEE'],
            ['description' => 'Найдена критическая ошибка', 'color' => '#FF6B5B'],
            ['description' => 'Найдена ошибка ср. тяжести', 'color' => '#FAA798'],
            ['description' => 'Найдена незначительная ошибка', 'color' => '#F5D5D1'],
            ['description' => 'Ошибочный ввод данных', 'color' => '#DBE6F7'],
            ['description' => 'Визуальные улучшения', 'color' => '#DBE6F7'],
            ['description' => 'Расширение функционала', 'color' => '#FCD69C'],
            ['description' => 'Расширение/уменьшение прав доступа', 'color' => '#FCD69C'],
            ['description' => 'Изменение функционала', 'color' => '#FCD69C'],
            ['description' => 'Другое', 'color' => '#FCD69C'],
            ['description' => 'Проблема с почтой', 'color' => '#DBFAC7'],
            ['description' => 'Не открывается сайт, проблема с ДНС', 'color' => '#DBFAC7'],
            ['description' => 'Авторизация - Wi-Fi', 'color' => '#DBFAC7'],
            ['description' => 'Виртуализация', 'color' => '#FAB8B8'],
            ['description' => 'Другое', 'color' => '#EEEEEE'],
                                               ]);

        DB::table('feedback_department_category')->insert([
            ['department_id' => 4, 'feedback_category_id' => 1],
            ['department_id' => 4, 'feedback_category_id' => 2],
            ['department_id' => 4, 'feedback_category_id' => 3],
            ['department_id' => 4, 'feedback_category_id' => 4],
            ['department_id' => 4, 'feedback_category_id' => 5],
            ['department_id' => 4, 'feedback_category_id' => 6],
            ['department_id' => 4, 'feedback_category_id' => 7],
            ['department_id' => 4, 'feedback_category_id' => 8],
            ['department_id' => 4, 'feedback_category_id' => 9],
            ['department_id' => 5, 'feedback_category_id' => 19],
            ['department_id' => 5, 'feedback_category_id' => 20],
            ['department_id' => 5, 'feedback_category_id' => 21],
            ['department_id' => 5, 'feedback_category_id' => 22],
            ['department_id' => 5, 'feedback_category_id' => 23],
            ['department_id' => 6, 'feedback_category_id' => 10],
            ['department_id' => 6, 'feedback_category_id' => 11],
            ['department_id' => 6, 'feedback_category_id' => 12],
            ['department_id' => 6, 'feedback_category_id' => 13],
            ['department_id' => 6, 'feedback_category_id' => 14],
            ['department_id' => 6, 'feedback_category_id' => 15],
            ['department_id' => 6, 'feedback_category_id' => 16],
            ['department_id' => 6, 'feedback_category_id' => 17],
            ['department_id' => 6, 'feedback_category_id' => 18],
                                                          ]);
    }
}
