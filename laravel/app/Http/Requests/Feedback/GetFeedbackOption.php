<?php

namespace App\Http\Requests\Feedback;

use Illuminate\Foundation\Http\FormRequest;

class GetFeedbackOption extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'taskView' => 'required|in:open,close,pause',
            'taskForDepartmentId' => 'required|in:4,5,6'
        ];
    }

    public function messages()
    {
      return [
        'taskView.required' => 'Необходимо указать вид задач',
        'taskView.in' => 'Неверный вид задачи',
        'taskForDepartmentId.exists' => 'ID отдела не может быть пустым',
        'taskForDepartmentId.in' => 'Неверный ID отдела',
      ];
    }
}
