<?php

namespace App\Http\Middleware;

use Closure;

class Permission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $exp)
    {
        if (!checkPermissionExpression($request, $exp))
            abort(401, 'Отсутствуют права доступа');

        return $next($request);
    }
}
