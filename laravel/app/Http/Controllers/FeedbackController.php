<?php

namespace App\Http\Controllers;

use App\Helpers\Breadcrumb;
use App\Http\Requests\Feedback\AddFeedback;
use App\Http\Requests\Feedback\GetFeedbackOption;
use App\Http\Requests\Feedback\EditFeedback;
use App\Models\Feedback\FeedbackStatus;
use App\Models\Feedback\FeedbackDepartmentCategory;
use Illuminate\Http\Request;
use App\Models\Feedback\Feedback;
use App\Models\Feedback\FeedbackCategory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class FeedbackController extends Controller
{
    public $priorityList = [['name'  => 'Низкий',
                             'value' => 'low',
                             'class' => 'btn-success'],
                            ['name'  => 'Средний',
                             'value' => 'middle',
                             'class' => 'btn-warning'],
                            ['name'  => 'Высокий',
                             'value' => 'high',
                             'class' => 'btn-danger'],
                            ];

    public function index(GetFeedbackOption $request)
    {
        Breadcrumb::push('Обратная связь', '/feedback?taskForDepartmentId=6&taskView=open');
        //Если в get прилетают параметры
        if($request->all())
        {
            $taskView = $request->all()['taskView'];
            $taskForDepartmentId = $request->all()['taskForDepartmentId'];
        }
        else //Если get пустой, то ставим дефолтные значения
        {
            $taskView = "open";
            $taskForDepartmentId = 6;
        }

        //Выбираем вид задачи: открытые, отложенные, закрытые
        $whereStatus = [];
        if($taskView == 'open')
            $whereStatus = [1,2,3,7,9];
        elseif ($taskView == 'pause')
            $whereStatus = [6];
        elseif ($taskView == 'close')
            $whereStatus = [4,5,8];

        $feedback = Feedback::with('category','status','author','executingUser')->where('department_id', $taskForDepartmentId)
                                                                                ->whereIn('feedback_status_id', $whereStatus)
                                                                                ->paginate(5);
        return view('feedback.feedback',['feedback' => $feedback,
                                         'departmentList' => $this->getDepartmentList(),
                                         'taskView' => $taskView,
                                         'taskForDepartmentId' => $taskForDepartmentId,
                                        ]);
    }

    public function viewFeedback(Request $request)
    {
        Breadcrumb::push('Обратная связь', '/feedback?taskForDepartmentId=6&taskView=open');
        Breadcrumb::push('Обращение # '.$request->id);
        $feedbackView = Feedback::with('category','status','comment','file','author','executingUser','follower','subtask')->where('id',$request->id)->get()->toArray()[0];
        $categoryList = $this->getCategoryListByDepartmentId($feedbackView['department_id'],false)->toArray();
        $statusList = $this->getStatusList();
        $departmentList = $this->getDepartmentList();

        return view('feedback.viewFeedback',['feedbackView' => $feedbackView,
                                             'categoryList' => $categoryList,
                                             'priorityList' => $this->priorityList,
                                             'statusList'   => $statusList,
                                             'departmentList' => $departmentList,
                                            ]);
    }

    public function addFeedback(AddFeedback $request)
    {

        $id = DB::table('feedback')->insertGetId(
                ['department_id' => $request->input('departmentId'),
                 'feedback_category_id' => $request->input('categoryId'),
                 'feedback_status_id' => 1,
                 'author_id' => Auth::user()->id,
                 'executing_user_id' => NULL,
                 'priority' => $request->input('priority'),
                 'title' => $request->input('title'),
                 'description' => $request->input('description'),
                 'open_date' => date("Y-m-d H-i-s"),
                ]);
        echo json_encode($id);
    }

    public function editFeedback(EditFeedback $request)
    {
        DB::table('feedback')->where('id',$request->input('feedbackId'))
                             ->update([
                                       'feedback_category_id' => $request->input('feedbackCategory'),
                                       'feedback_status_id'   => $request->input('feedbackStatus'),
                                       'executing_user_id'    => $request->input('executingUserId'),
                                      ]) ;

        return redirect('./feedback/viewFeedback?id='.$request->input('feedbackId'));
    }

    public function getDepartmentList()
    {
        $departmentList = FeedbackDepartmentCategory::select('feedback_department_category')
                           ->distinct()
                           ->select('department.id',
                                    'department.name')
                           ->leftJoin('department','feedback_department_category.department_id','=','department.id')
                           ->orderBy('department.id', 'desc')
                           ->get()->toArray();

        return $departmentList;
    }


    public function getCategoryListByDepartmentId($id,$json=true)
    {
        $categoryByDepartment = DB::table('feedback_department_category')
                                 ->select('feedback_category.id',
                                          'feedback_category.description',
                                          'feedback_category.color')
                                 ->leftJoin('feedback_category','feedback_department_category.feedback_category_id','=','feedback_category.id')
                                 ->where('feedback_department_category.department_id','=',$id)
                                 ->get();

        if($json)
            return json_encode($categoryByDepartment);
        else
            return $categoryByDepartment;
    }

    public function getStatusList()
    {
        $statusList = FeedbackStatus::all()->toArray();

        return $statusList;
    }

}
