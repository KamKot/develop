<?php

namespace App\Models\Feedback;

use Illuminate\Database\Eloquent\Model;

class FeedbackDepartmentCategory extends Model
{
    public $timestamps = false;
    protected $table = 'feedback_department_category';
}
