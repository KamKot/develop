<?php

namespace App\Models\Feedback;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    public $timestamps = false;
    protected $table = 'feedback';

    public function category()
    {
        return $this->hasOne('App\Models\Feedback\FeedbackCategory','id','feedback_category_id');
    }

    public function comment()
    {
        return $this->hasMany('App\Models\Feedback\FeedbackComment','feedback_id','id');
    }

    public function file()
    {
        return $this->hasMany('App\Models\Feedback\FeedbackFile');
    }

    public function follower()
    {
        return $this->hasMany('App\Models\Feedback\FeedbackFollower');
    }

    public function status()
    {
        return $this->hasOne('App\Models\Feedback\FeedbackStatus','id','feedback_status_id');
    }

    public function subtask()
    {
        return $this->hasMany('App\Models\Feedback\FeedbackSubtask');
    }

    public function author()
    {
        return $this->hasOne('App\Models\User','id','author_id');
    }

    public function executingUser()
    {
        return $this->hasOne('App\Models\User','id','executing_user_id');
    }
}
