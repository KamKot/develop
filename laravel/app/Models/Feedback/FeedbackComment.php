<?php

namespace App\Models\Feedback;

use Illuminate\Database\Eloquent\Model;

class FeedbackComment extends Model
{
    public $timestamps = false;
    protected $table = 'feedback_comment';

}
