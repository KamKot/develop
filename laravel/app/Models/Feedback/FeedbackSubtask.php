<?php

namespace App\Models\Feedback;

use Illuminate\Database\Eloquent\Model;

class FeedbackSubtask extends Model
{
    public $timestamps = false;
    protected $table = 'feedback_subtask';
}
